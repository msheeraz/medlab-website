<!DOCTYPE html>
<html lang="en">
<head>
<title>Travel Agency Bootstrap responsive template (theme)</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">

<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="css/select2.css" rel="stylesheet">
<link href="css/smoothness/jquery-ui-1.10.0.custom.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">

<script src="js/jquery.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/superfish.js"></script>

<script src="js/select2.js"></script>

<script src="js/jquery.parallax-1.1.3.resize.js"></script>

<script src="js/SmoothScroll.js"></script>

<script src="js/jquery.appear.js"></script>

<script src="js/jquery.caroufredsel.js"></script>
<script src="js/jquery.touchSwipe.min.js"></script>

<script src="js/jquery.ui.totop.js"></script>

<script src="js/script.js"></script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="front">

<div id="main">

<div class="top1_wrapper">
  <div class="container">
    <div class="top1 clearfix">
      <div class="email1"><a href="#">ops@aviamaldives.com</a></div>
      <div class="phone1">+(960) 778 6419</div>
      <div class="social_wrapper">
        <ul class="social clearfix">
          <li><a href="#"><i class="fa fa-facebook"></i></a></li>
          <li><a href="#"><i class="fa fa-twitter"></i></a></li>
          <li><a href="#"><i class="fa fa-skype"></i></a></li>
          <li><a href="#"><i class="fa fa-instagram"></i></a></li>
        </ul>
      </div>
      <!-- <div class="lang1">
        <div class="dropdown">
          <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">English<span class="caret"></span>
          </button>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li><a class="ge" href="#">German</a></li>
            <li><a class="ru" href="#">Russian</a></li>
          </ul>
        </div>
      </div> -->
    </div>
  </div>
</div>

<div class="top2_wrapper">
  <div class="container">
    <div class="top2 clearfix">
      <header>
        <div class="logo_wrapper">
          <a href="{{ URL::to('/') }}/" class="logo">
            <img src="images/logo.png" alt="" class="img-responsive">
          </a>
        </div>
      </header>
      <div class="navbar navbar_ navbar-default">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <div class="navbar-collapse navbar-collapse_ collapse">
          <ul class="nav navbar-nav sf-menu clearfix">
            <li class="active"><a href="{{ URL::to('/') }}/">Home</a></li>
              <li class="sub-menu sub-menu-1"><a href="#">Tools<em></em></a>
                <ul>
                  <li><a href="search-flights.html">NOTAM</a></li>
                  <li><a href="search-flights.html">Weather Information</a></li>
                </ul>
              </li>
            <li class="sub-menu sub-menu-1"><a href="#">Services<em></em></a>
              <ul>
                <li><a href="flights.html">Ground Handling</a>
                <ul>
                    <li><a href="booking-flights-page.html">Jet Handling</a></li>
                    <li><a href="booking-flights-page.html">Flight Handling</a></li>
                    <li><a href="booking-flights-page.html">Air Embulance Handling</a></li>
                    <li><a href="booking-flights-page.html">Infligt Catering</a></li>
                </ul>
                </li>

                <li><a href="hotels.html">Travel and Tours</a>
                    <ul>
                        <li><a href="search-hotel.html">Tours & Guide</a></li>
                        <li><a href="booking-hotel.html">VIP Meet & Greet</a></li>
                        <li><a href="booking-hotel-page.html">Hotel Reservation</a></li>
                        <li><a href="booking-hotel-page.html">Air Ticketing</a></li>
                    </ul>
                </li>
              </ul>
            </li>
            <li><a href="downloads">Downloads</a></li>
            <li><a href="maldives">Maldives</a></li>
            <li><a href="contacts.html">About Us</a></li>
            <li><a href="contacts.html">Contact Us</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="slider_wrapper">
  <div class="container">
    <div id="slider_inner">
      <div class="">
        <div id="slider">

          <div class="">
            <div class="carousel-box">
              <div class="inner">
                <div class="carousel main">
                  <ul>
                    <li>
                      <div class="slider">
                        <div class="slider_inner">
                          <div class="txt2"><span>GROUND HANDLING</span></div>
                          <div class="txt3"><span>On the ramp we are the experts you are looking for, our team is swift and efficient in handling of flights and providing all the following services required safely and punctually.</span></div>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="slider">
                        <div class="slider_inner">
                          <div class="txt2"><span>INFLIGHT CATERING</span></div>
                          <div class="txt3"><span>The passengers of our clients enjoy and experience five star-quality service with carefully selected, high quality menus that are regularly changed in order to the preference of our clients.</span></div>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="slider">
                        <div class="slider_inner">
                          <div class="txt2"><span>PAX AND CREW CARE</span></div>
                          <div class="txt3"><span>The Passenger and Crew encounter with AVIA MALDIVES is more than just a handling operation, it is an experience in exceptional hospitality.</span></div>
                        </div>
                      </div>
                    </li>

                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="slider_pagination"></div>
        </div>
      </div>
    </div>
  </div>
</div>



<div id="why1">
  <div class="container">

    <h2 class="animated">WHO WE ARE</h2>

    <div class="title1 animated">Avia Maldives, our goal is to go above and beyond to provide the amenities, service and attention our clients need when and where our clients need them.</div>

    <br><br>

    <div class="row" id="isonic">
      <div class="col-sm-3">
        <div class="thumb2 animated" data-animation="flipInY" data-animation-delay="200">
          <div class="thumbnail clearfix">
            <a href="#">
              <figure class="">
                <img src="images/icon_flighthandling.png" alt="" class="img1 img-responsive">
                <img src="images/icon_flighthandlingH.png" alt="" class="img2 img-responsive">
              </figure>
              <div class="caption">
                <div class="txt1">Flight Handling</div>
                <div class="txt2">We takecare of your journey right from touch down till airbourne</div>
                <div class="txt3">Read More</div>
              </div>
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="thumb2 animated" data-animation="flipInY" data-animation-delay="300">
          <div class="thumbnail clearfix">
            <a href="#">
              <figure class="">
                <img src="images/icon_tools.png" alt="" class="img1 img-responsive">
                <img src="images/icon_toolsH.png" alt="" class="img2 img-responsive">
              </figure>
              <div class="caption">
                <div class="txt1">Tools</div>
                <div class="txt2">Handful of tools for your flight planning.</div>
                <div class="txt3">Read More</div>
              </div>
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="thumb2 animated" data-animation="flipInY" data-animation-delay="400">
          <div class="thumbnail clearfix">
            <a href="#">
              <figure class="">
                <img src="images/icon_hotel.png" alt="" class="img1 img-responsive">
                <img src="images/icon_hotelH.png" alt="" class="img2 img-responsive">
              </figure>
              <div class="caption">
                <div class="txt1">Hotel Booking</div>
                <div class="txt2">Let us help you to book perfect hotel of choice for your stay.</div>
                <div class="txt3">Read More</div>
              </div>
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="thumb2 animated" data-animation="flipInY" data-animation-delay="500">
          <div class="thumbnail clearfix">
            <a href="#">
              <figure class="">
                <img src="images/icon_airline.png" alt="" class="img1 img-responsive">
                <img src="images/icon_airlineH.png" alt="" class="img2 img-responsive">
              </figure>
              <div class="caption">
                <div class="txt1">Airline Ticketing</div>
                <div class="txt2">We provide airline ticketing including domestic transport.</div>
                <div class="txt3">Read More</div>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="parallax1" class="parallax">
  <div class="bg1 parallax-bg"></div>
  <div class="overlay"></div>
  <div class="parallax-content">
    <div class="container">

      <div class="row">
        <div class="col-sm-12 animated">
          <div class="txt1">" Right fom start to end, if was a smooth ride with you Avia Maldives. "</div>
          <div class="txt3">-Khazbegi (Goergia) / MJets</div>
        </div>
      </div>

    </div>
  </div>
</div>

<div id="popular_cruises1">
  <div class="container">

    <h2 class="animated">OUR SERVICES</h2>

    <div class="title1 animated" style="width:40em; margin:0px auto">We pride ourselves on developing personal relationships with our customers, our operator and reinforce our strict quality and safety standards affiliated to Europe Business Aviation Association (EBAA), Middle East Business Aviation Association (MEBAA) and National Business Aviation Association (NBAA).</div>

    <br><br>

    <div id="popular_wrapper" class="animated" data-animation="fadeIn" data-animation-delay="300">
      <div id="popular_inner">
        <div class="">
          <div id="popular">
            <div class="">
              <div class="carousel-box">
                <div class="inner">
                  <div class="carousel main">
                    <ul>
                      <li>
                        <div class="popular">
                          <div class="popular_inner">
                            <figure>
                              <img src="http://localhost/avia/library/inners/jet_handling.jpg" alt="" class="img-responsive">
                            </figure>
                            <div class="caption">
                              <div class="txt1"><span>Jet Handling</span></div>
                              <div class="txt2">Our experienced team continuosly work hard to meet your requirement and provide a remarkable service for your Jet operations.</div>
                              <div class="txt3 clearfix">
                                <div class="right_side"><a href="#" class="btn-default btn1">More</a></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li><div class="popular">
                        <div class="popular_inner">
                          <figure>
                            <img src="http://localhost/avia/library/inners/inflight_catering.jpg" alt="" class="img-responsive">
                          </figure>
                          <div class="caption">
                            <div class="txt1"><span>Inflight Catering</span></div>
                            <div class="txt2">We provide highest quality catering for VIP and other customers.</div>
                            <div class="txt3 clearfix">
                              <div class="right_side"><a href="#" class="btn-default btn1">More</a></div>
                            </div>
                          </div>
                        </div>
                      </div>
                      </li>
                      <li>
                        <div class="popular">
                          <div class="popular_inner">
                            <figure>
                              <img src="http://localhost/avia/library/inners/airembulance_handling.jpg" alt="" class="img-responsive">
                            </figure>
                            <div class="caption">
                              <div class="txt1"><span>Air Embulance Handling</span></div>
                              <div class="txt2">We provide Air Embulance service from and to Maldives.</div>
                              <div class="txt3 clearfix">
                                <div class="right_side"><a href="#" class="btn-default btn1">More</a></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="popular">
                          <div class="popular_inner">
                            <figure>
                              <img src="http://localhost/avia/library/inners/flight_handling.jpg" alt="" class="img-responsive">
                            </figure>
                            <div class="caption">
                              <div class="txt1"><span>Flight Handling</span></div>
                              <div class="txt2">We provide flight handling related services such as Ramp Handling, Navigational Aids, Passenger/Crew Escort.</div>
                              <div class="txt3 clearfix">
                                <div class="right_side"><a href="#" class="btn-default btn1">More</a></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="popular">
                          <div class="popular_inner">
                            <figure>
                              <img src="http://localhost/avia/library/inners/tours.jpg" alt="" class="img-responsive">
                            </figure>
                            <div class="caption">
                              <div class="txt1"><span>Tours & Guide</span></div>
                              <div class="txt2">Whether you are plaaning a vacation or adventure, let us help you to cutomize your trip.</div>
                              <div class="txt3 clearfix">
                                <div class="right_side"><a href="#" class="btn-default btn1">More</a></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="popular">
                          <div class="popular_inner">
                            <figure>
                              <img src="http://localhost/avia/library/inners/vip.jpg" alt="" class="img-responsive">
                            </figure>
                            <div class="caption">
                              <div class="txt1"><span>VIP Meet & Greet</span></div>
                              <div class="txt2">Whether you�re a high-profile executive, famed entertainer, or simply have unique travel needs, Gateway VIP Services will ensure that your travel experience is effortless and flawless.</div>
                              <div class="txt3 clearfix">
                                <div class="right_side"><a href="#" class="btn-default btn1">More</a></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="popular">
                          <div class="popular_inner">
                            <figure>
                              <img src="http://localhost/avia/library/inners/hotel.jpg" alt="" class="img-responsive">
                            </figure>
                            <div class="caption">
                              <div class="txt1"><span>Hotel Reservation</span></div>
                              <div class="txt2">Let us help you with hotel reservation across Maldives starting from Luxury Resort Island to Guest Houses.</div>
                              <div class="txt3 clearfix">
                                <div class="right_side"><a href="#" class="btn-default btn1">More</a></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="popular">
                          <div class="popular_inner">
                            <figure>
                              <img src="http://localhost/avia/library/inners/ticketing.jpg" alt="" class="img-responsive">
                            </figure>
                            <div class="caption">
                              <div class="txt1"><span>Air Ticketing</span></div>
                              <div class="txt2">We provide air ticketing services and help find best deal for your travel needs.</div>
                              <div class="txt3 clearfix">
                                <div class="right_side"><a href="#" class="btn-default btn1">More</a></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>

                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="popular_pagination"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div id="parallax3" class="parallax">
  <div class="bg2 parallax-bg"></div>
  <div class="overlay"></div>
  <div class="parallax-content">
    <div class="container">

      <div class="row">
        <div class="col-sm-12 col-md-12">
          <div class="content" style="background-color:rgba(9, 59, 130, 0.87); padding:2em">
            <div class="txt1 animated" data-animation="fadeIn" data-animation-delay="100">HAPPY CUSTOMERS <span>2017</span></div>

            <div class="txt3 animated" data-animation="fadeIn" data-animation-delay="200">
              <p>
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh oui-
  sod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit dolore magna aliquam erat voutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit..
              </p>

            </div>

            <div class="distance1 animated" data-animation="fadeInUp" data-animation-delay="0">
              <div class="txt">Flights Handled</div>
              <div class="bg">
                <div class="animated-distance" data-num="94" data-duration="1300" data-animation-delay="0"><span></span></div>
              </div>
            </div>

            <div class="distance1 animated" data-animation="fadeInUp" data-animation-delay="100">
              <div class="txt">Hotels Booked</div>
              <div class="bg">
                <div class="animated-distance" data-num="87" data-duration="1300" data-animation-delay="100"><span></span></div>
              </div>
            </div>

            <div class="distance1 animated" data-animation="fadeInUp" data-animation-delay="200">
              <div class="txt">Tours Arranged</div>
              <div class="bg">
                <div class="animated-distance" data-num="48" data-duration="1300" data-animation-delay="200"><span></span></div>
              </div>
            </div>

            <div class="distance1 animated" data-animation="fadeInUp" data-animation-delay="300">
              <div class="txt">Caterings Arranged</div>
              <div class="bg">
                <div class="animated-distance" data-num="51" data-duration="1300" data-animation-delay="300"><span></span></div>
              </div>
            </div>

          </div>
        </div>
      </div>

    </div>
  </div>
</div>



<div id="partners">
  <div class="container">
    <div class="row">
      <div class="col-sm-4 col-md-2 col-xs-6">
        <div class="thumb1 animated">
          <div class="thumbnail clearfix">
            <a href="#">
              <figure>
                <img src="http://placehold.it/170x95" alt="" class="img1 img-responsive">
                <img src="http://placehold.it/170x95" alt="" class="img2 img-responsive">
              </figure>
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4 col-md-2 col-xs-6">
        <div class="thumb1 animated">
          <div class="thumbnail clearfix">
            <a href="#">
              <figure>
                <img src="http://placehold.it/170x95" alt="" class="img1 img-responsive">
                <img src="http://placehold.it/170x95" alt="" class="img2 img-responsive">
              </figure>
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4 col-md-2 col-xs-6">
        <div class="thumb1 animated">
          <div class="thumbnail clearfix">
            <a href="#">
              <figure>
                <img src="http://placehold.it/170x95" alt="" class="img1 img-responsive">
                <img src="http://placehold.it/170x95" alt="" class="img2 img-responsive">
              </figure>
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4 col-md-2 col-xs-6">
        <div class="thumb1 animated">
          <div class="thumbnail clearfix">
            <a href="#">
              <figure>
                <img src="http://placehold.it/170x95" alt="" class="img1 img-responsive">
                <img src="http://placehold.it/170x95" alt="" class="img2 img-responsive">
              </figure>
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4 col-md-2 col-xs-6">
        <div class="thumb1 animated">
          <div class="thumbnail clearfix">
            <a href="#">
              <figure>
                <img src="http://placehold.it/170x95" alt="" class="img1 img-responsive">
                <img src="http://placehold.it/170x95" alt="" class="img2 img-responsive">
              </figure>
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-4 col-md-2 col-xs-6">
        <div class="thumb1 animated">
          <div class="thumbnail clearfix">
            <a href="#">
              <figure>
                <img src="http://placehold.it/170x95" alt="" class="img1 img-responsive">
                <img src="http://placehold.it/170x95" alt="" class="img2 img-responsive">
              </figure>
            </a>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<div class="bot1_wrapper">
  <div class="container">
    <div class="row">
      <div class="col-sm-3">
        <div class="logo2_wrapper">
          <a href="{{ URL::to('/') }}/" class="logo">
            <img src="images/logo.png" alt="" class="img-responsive">
          </a>
        </div>
        <ul id="company_contact">
          <li>Phone : + (960) 300 0058</li>
          <li>Fax : + (960) 300 0057 </li>
          <li>Email : ops@aviamaldives.com</li>
          <li>SITA : MLEAVXH</li>
          <li>ARINC : MLEAMXH</li>
          <li>AFTN : KMYVXAAA</li>

        </ul>
      </div>
      <div class="col-sm-3">
        <div class="bot1_title">Our Instagram</div>
        <ul class="ul1">
          <li><a href="#">First Class Flights</a></li>
          <li><a href="#">Accessible Travel</a></li>
          <li><a href="#">Amazing Cruises</a></li>
        </ul>

        <div class="social2_wrapper">
          <ul class="social2 clearfix">
            <li class="nav1"><a href="#"></a></li>
            <li class="nav2"><a href="#"></a></li>
            <li class="nav3"><a href="#"></a></li>
            <li class="nav4"><a href="#"></a></li>
            <li class="nav5"><a href="#"></a></li>
            <li class="nav6"><a href="#"></a></li>
          </ul>
        </div>

      </div>
      <div class="col-sm-3">
        <div class="bot1_title">Our Twitter</div>
        <div class="twits1">
          <div class="twit1">
           <a href="#"> @travel</a> Lorem ipsum dolor sit amet, consectetuer adipiscing elit
           <div class="date">5 minutes ago</div>
          </div>
          <div class="twit1">
           <a href="#">@leo</a> Nam liber tempor cum soluta nobis option congue nihil imperdiet doming id quod mazim.
           <div class="date">2 days ago</div>
          </div>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="bot1_title">Newsletter</div>
        <div class="newsletter_block">
          <div class="txt1">Inspiration, ideas, news and your feedback.</div>
          <div class="newsletter-wrapper clearfix">
          <form class="newsletter" action="javascript:;">
            <input type="text" name="s" value='Email Address' onBlur="if(this.value=='') this.value='Email Address'" onFocus="if(this.value =='Email Address' ) this.value=''">
            <a href="#" class="btn-default btn3">SUBMIT</a>
          </form>
        </div>
        </div>
        <div class="phone2">1-917-338-6831</div>
        <div class="support1"><a href="#">support@templates-support.com</a></div>
      </div>
    </div>
  </div>
</div>

<div class="bot2_wrapper">
  <div class="container">
    <div class="left_side">
      Copyright � 2017 <strong>AVIA MALDIVES PVT LTD</strong>   <span>|</span>   <a href="#">About Us</a>   <span>|</span>   <a href="#">Contact Support</a>
    </div>
  </div>
</div>






</div>
<script src="js/bootstrap.min.js"></script>
</body>
</html>
