@extends('masterlayout')

@section('spc_header')
  <meta name="description" content="Medlab Diagnostics Private Limited formerly known as Medlab Diagnostics is formed for a specialty for medical devices and lab consumables and Chemicals to sell products to companies and market and introduce latest medical devices, related any surgical niche market within Maldives. "/>
  <meta name="keywords" content="Medlab, Diagnostics, Maldives"/>
@stop

@section('styles_header')
  <style media="screen">
  .popover {
    background-color:#252525;

  }
  .popover.bottom .arrow:after {
      border-bottom-color: #252525;
  }
  .popover.top {
      border-bottom-color: #252525;
  }
  .pagination > li > a, .pagination > li > span {
    position: relative;
    float: left;
    padding: 6px 12px;
    margin-left: -1px;
    line-height: 1.42857143;
    color: #FFF;
    text-decoration: none;
    background-color: #d4393d;
    border: 1px solid #ddd;
  }
  .pagination > li > a:hover  {
    color: #FFF;
    text-decoration: none;
    background-color: #06273c;
  }
  </style>
@stop


@section('spc_footer')

@stop

@section('topnavi')
  @include('news.topnavi')
@stop


@section('content')
<div id="main">
<div class="top1_wrapper">
  <div class="container">
    <div class="top1 clearfix">
      <div class="email1"><a href="#">info@medlabdiognostics.com</a></div>
      <div class="phone1">+(960) 3010878</div>
      <div class="social_wrapper">
        <ul class="social clearfix">
          <li><a href="https://www.facebook.com/pages/Medlab-Diagnostics-Pvt-Ltd/705679799583849" target="_blank"><i class="fa fa-facebook"></i></a></li>
          <li><a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a></li>
          <li><a href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>


@yield('topnavi')





<div id="why1">
  <div class="container" style="min-height:500px">

    <h2 class="animated" style="float:left">
      NEWS
    </h2>


     <div id="cont_pagination" class="pull-right">
         <div style="float: left; width: 180px;"> {{ $data_news->links() }} </div>
         <div style="float: right; text-align: right; font-size: 15px; margin-top: 25px; margin-right: 10px; width: 127px; font-weight: bold">Showing {{($data_news->currentpage()-1)*$data_news->perpage()+1}} to {{$data_news->currentpage()*$data_news->perpage()}}</div>
      </div>
      <div class="col-md-12">
        <ul class="ul4">
          @if(!$data_news->isEmpty())

          <div id="popular_wrapper" class="animated" data-animation="fadeIn" data-animation-delay="300">
            <div id="popular_inner">
              <div class="row">
                @foreach($data_news as $details)
                <div class="col-md-3 col-xs-6 col-sm-2">
                  <div class="popular">
                      <div class="popular_inner" style="margin-bottom:10px">
                        <figure>
                          <img src="{{ URL::to('/') }}/library/news/200x100/{{ $details->img }}" alt="" class="img-responsive">
                        </figure>
                        <div class="caption" style="min-height:250px">
                          <div class="txt1"><span>{{ $details->title }}</span></div>
                          <div class="txt2"><strong>{{ date('d M Y', strtotime($details->entrydate)) }} </strong> - {{ $details->description }}</div>
                          <div class="txt3 clearfix">
                            <div class="right_side"><a href="{{ URL::to('/') }}/news/details/{{ $details->incre }}" class="btn-default btn1">Details</a></div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
                @endforeach
              </div>
            </div>
          </div>

          @endif
          </ul>

      </div>


</div>
</div>





<div class="bot1_wrapper">
<div class="container">
  <div class="row">
    <div class="col-sm-3">
      <div class="logo2_wrapper">
        <a href="{{ URL::to('/') }}/" class="logo">
          <img src="{{ URL::to('/') }}/images/logo.png" alt="" class="img-responsive">
          <img src="{{ URL::to('/') }}/images/190x180/iso.png" alt="" class="img-responsive">
        </a>
      </div>
    </div>
    <div class="col-sm-3">
      <div class="bot1_title">Our Address</div>
      <div style="line-height:25px">
          Address : MED LAB DIAGNOSTICS PRIVATE LIMITED HEAD OFFICE<br>
          H.DHUBUGASDOSUGE <br>
          KURAGI GOLHI, HENVEIRU<br>
          PO BOX 200026, <br>
          MALE’ REPUBLIC OF MALDIVES
      </div>
    </div>
    <div class="col-sm-3">
      <div class="bot1_title">Our Contact</div>
      <div style="line-height:25px">
        Phone : + (960) 3010878<br>
        Fax : + (960) 3010877 <br>
        Email : ops@aviamaldives.com<br>
        ISO Certificatio No. : 32665-A01<br>
      </div>
    </div>
    <div class="col-sm-3">
      <div class="bot1_title">Social Media</div>
      <div>
        <ul class="social clearfix">
          <li><a style="font-size:35px; color:#3b5998" href="https://www.facebook.com/pages/Medlab-Diagnostics-Pvt-Ltd/705679799583849" target="_blank"><i class="fa fa-facebook"></i></a></li>
          <li><a style="font-size:35px; color:#29a2f1"href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a></li>
          <li><a style="font-size:35px; color:#b83289"href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
</div>

<div class="bot2_wrapper">
<div class="container">
  <div class="left_side">
    Copyright &copy; <?=date("Y");?> <strong>MedLab Diagnostics Pvt Ltd</strong>   <span>|</span>   <a href="{{ URL::to('/') }}/about">About Us</a>   <span>
  </div>
</div>
</div>
</div>
@stop
