@section('topnavi')
<div class="top2_wrapper">
  <div class="container">
    <div class="top2 clearfix">
      <header>
        <div class="logo_wrapper">
          <a href="{{ URL::to('/') }}/" class="logo">
            <img src="{{ URL::to('/') }}/images/logo.png" alt="" class="img-responsive">
          </a>
        </div>
      </header>
      <div class="navbar navbar_ navbar-default">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <div class="navbar-collapse navbar-collapse_ collapse">
          <ul class="nav navbar-nav sf-menu clearfix">
            <li><a href="{{ URL::to('/') }}/">Home</a></li>
            <li class="sub-menu sub-menu-1"><a href="{{ URL::to('/') }}/products">Products</a></li>
            <li class="sub-menu sub-menu-1"><a href="#">Services<em></em></a>
              <ul>
                <li><a href="{{ URL::to('/') }}/services/laboratary">Laboratory Services</a></li>
                <li><a href="{{ URL::to('/') }}/services/cliniclabfacilities">Advance Clinic Lab Facilities</a></li>
                <li><a href="{{ URL::to('/') }}/services/wholesalesupply">Wholesale & Supply</a></li>
                <li><a href="{{ URL::to('/') }}/services/corporate">Corporate Responsibility</a></li>
              </ul>
            </li>
            <li class="active"><a href="{{ URL::to('/') }}/news">News</a></li>
            <li><a href="{{ URL::to('/') }}/downloads">Downloads</a></li>
            <li><a href="{{ URL::to('/') }}/careers">Careers</a></li>
            <li><a href="{{ URL::to('/') }}/about">About Us</a></li>
            <li><a href="{{ URL::to('/') }}/contact">Contact Us</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
