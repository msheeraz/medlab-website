  <!DOCTYPE html>
  <html lang="en">
  <head>
  <title>MedLab Diagnostics pvt ltd | Welcome</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" type="image/x-icon" href="{{ URL::to('/') }}/images/logo.png">
  @yield('spc_header')
  <link href="{{ URL::to('/') }}/css/bootstrap.css" rel="stylesheet">
  <link href="{{ URL::to('/') }}/css/font-awesome.css" rel="stylesheet">
  <link href="{{ URL::to('/') }}/css/animate.css" rel="stylesheet">
  <link href="{{ URL::to('/') }}/css/select2.css" rel="stylesheet">
  <link href="{{ URL::to('/') }}/css/smoothness/jquery-ui-1.10.0.custom.css" rel="stylesheet">
  <link href="{{ URL::to('/') }}/css/style.css?ref=55" rel="stylesheet">
  @yield('styles_header')

  <script src="{{ URL::to('/') }}/js/jquery.js"></script>
  <script src="{{ URL::to('/') }}/js/jquery-ui.js"></script>
  <script src="{{ URL::to('/') }}/js/jquery-migrate-1.2.1.min.js"></script>
  <script src="{{ URL::to('/') }}/js/jquery.easing.1.3.js"></script>
  <script src="{{ URL::to('/') }}/js/superfish.js"></script>
  <script src="{{ URL::to('/') }}/js/bootstrap.min.js"></script>
  <script src="{{ URL::to('/') }}/js/select2.js"></script>

  <script src="{{ URL::to('/') }}/js/jquery.parallax-1.1.3.resize.js"></script>

  <script src="{{ URL::to('/') }}/js/SmoothScroll.js"></script>

  <script src="{{ URL::to('/') }}/js/jquery.appear.js"></script>

  <script src="{{ URL::to('/') }}/js/jquery.caroufredsel.js"></script>
  <script src="{{ URL::to('/') }}/js/jquery.touchSwipe.min.js"></script>

  <script src="{{ URL::to('/') }}/js/jquery.ui.totop.js"></script>

  <script src="{{ URL::to('/') }}/js/script.js"></script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  </head>
  <body class="front">
    @yield('content')
    @yield('spc_sitemap')
    <script type="text/javascript">
      $(document).ready(function(){
        $('[data-toggle="popover"]').popover();
      })
      $(document).ajaxComplete(function() {
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover({
            placement : 'top',
            trigger : 'hover',
            html : true,
        });
        $('[data-toggle="wpopover"]').popover({
            trigger : 'hover',
            html : true,
            placement: 'top',
        });
      });
    </script>
    @yield('spc_footer')
   </body>
</html>
