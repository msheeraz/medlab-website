@extends('masterlayout')

@section('spc_header')
    <meta name="description" content="index desc"/>
    <meta name="keywords" content="index keywords"/>
@stop

@section('styles_header')
  <style media="screen">
  .popover {
    background-color:#252525;

  }
  .popover.bottom .arrow:after {
      border-bottom-color: #252525;
  }
  .popover.top {
      border-bottom-color: #252525;
  }
  </style>
@stop


@section('spc_footer')

@stop

@section('topnavi')
  @include('downloads.topnavi')
@stop


@section('content')
<div id="main">
<div class="top1_wrapper">
  <div class="container">
    <div class="top1 clearfix">
      <div class="email1"><a href="#">info@medlabdiognostics.com</a></div>
      <div class="phone1">+(960) 3010878</div>
      <div class="social_wrapper">
        <ul class="social clearfix">
          <li><a href="https://www.facebook.com/pages/Medlab-Diagnostics-Pvt-Ltd/705679799583849" target="_blank"><i class="fa fa-facebook"></i></a></li>
          <li><a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a></li>
          <li><a href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>


@yield('topnavi')





<div id="why1">
  <div class="container" style="min-height:500px">

    <h2 class="animated" style="float:left">
      DOWNLOADS
    </h2>



      <div class="col-md-12">
        <ul class="ul4">
          @if(!$data_downloads->isEmpty())
          @foreach($data_downloads as $info)
            <li><a target="_blank" href="{{ URL::to('/') }}/library/downloads/{!!html_entity_decode($info->file)!!}">{!!html_entity_decode($info->title)!!}<span>DOWNLOAD</span></a></li>
          @endforeach
          @endif
          </ul>

      </div>


</div>
</div>





<div class="bot1_wrapper">
<div class="container">
  <div class="row">
    <div class="col-sm-3">
      <div class="logo2_wrapper">
        <a href="{{ URL::to('/') }}/" class="logo">
          <img src="{{ URL::to('/') }}/images/logo.png" alt="" class="img-responsive">
          <img src="{{ URL::to('/') }}/images/190x180/iso.png" alt="" class="img-responsive">
        </a>
      </div>
    </div>
    <div class="col-sm-3">
      <div class="bot1_title">Our Address</div>
      <div style="line-height:25px">
          Address : MED LAB DIAGNOSTICS PRIVATE LIMITED HEAD OFFICE<br>
          H.DHUBUGASDOSUGE <br>
          KURAGI GOLHI, HENVEIRU<br>
          PO BOX 200026, <br>
          MALE’ REPUBLIC OF MALDIVES
      </div>
    </div>
    <div class="col-sm-3">
      <div class="bot1_title">Our Contact</div>
      <div style="line-height:25px">
        Phone : + (960) 3010878<br>
        Fax : + (960) 3010877 <br>
        Email : ops@aviamaldives.com<br>
        ISO Certificatio No. : 32665-A01<br>
      </div>
    </div>
    <div class="col-sm-3">
      <div class="bot1_title">Social Media</div>
      <div>
        <ul class="social clearfix">
          <li><a style="font-size:35px; color:#3b5998" href="https://www.facebook.com/pages/Medlab-Diagnostics-Pvt-Ltd/705679799583849" target="_blank"><i class="fa fa-facebook"></i></a></li>
          <li><a style="font-size:35px; color:#29a2f1"href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a></li>
          <li><a style="font-size:35px; color:#b83289"href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
</div>

<div class="bot2_wrapper">
<div class="container">
  <div class="left_side">
    Copyright &copy; <?=date("Y");?> <strong>MedLab Diagnostics Pvt Ltd</strong>   <span>|</span>   <a href="{{ URL::to('/') }}/about">About Us</a>   <span>
  </div>
</div>
</div>
</div>
@stop
