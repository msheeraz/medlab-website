@extends('masterlayout')

@section('spc_header')
@if(!$data_service->isEmpty())
    @foreach($data_service as $details)
        <meta name="description" content="{{ $details->description }}"/>
        <meta name="keywords" content="{{ $details->title }}"/>
    @endforeach
@endif
@stop

@section('styles_header')
  <style media="screen">
  .popover {
    background-color:#252525;

  }
  .popover.bottom .arrow:after {
      border-bottom-color: #252525;
  }
  .popover.top {
      border-bottom-color: #252525;
  }
  </style>
@stop


@section('spc_footer')
  <script type="text/javascript">
    $(document).ready(function(){

        $("#insta").css("line-height","0").css("-webkit-column-count", "5").css("-webkit-column-gap","0px").css("-moz-column-count","5").css("-moz-column-gap","0px").css("column-count","5").css("column-gap","0px");
        $.ajax({url: "{{ URL::to('/') }}/getinstagramfeeds", success: function(result){
              $("#insta").html(result + "<br>");
        }});

        $("#twitter").css("-webkit-column-count", "5").css("-webkit-column-gap","0px").css("-moz-column-count","5").css("-moz-column-gap","0px").css("column-count","5").css("column-gap","0px");
        $.ajax({url: "{{ URL::to('/') }}/gettwitterfeeds", success: function(result){
              $("#twitter").html(result + "<br>");
        }});


    })
  </script>
@stop

@section('topnavi')
  @include('services.topnavi')
@stop


@section('content')
<div id="main">
<div class="top1_wrapper">
  <div class="container">
    <div class="top1 clearfix">
      <div class="email1"><a href="#">info@medlabdiognostics.com</a></div>
      <div class="phone1">+(960) 3010878</div>
      <div class="social_wrapper">
        <ul class="social clearfix">
          <li><a href="https://www.facebook.com/pages/Medlab-Diagnostics-Pvt-Ltd/705679799583849" target="_blank"><i class="fa fa-facebook"></i></a></li>
          <li><a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a></li>
          <li><a href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>

@yield('topnavi')





<div id="why1">
  <div class="container">

    <h2 class="animated" style="float:left">
      @if(!$data_service->isEmpty())
      @foreach($data_service as $info)
        {!!html_entity_decode($info->title)!!}
      @endforeach
      @endif
    </h2>

    <div style="clear:both">
      @if(!$data_service->isEmpty())
      @foreach($data_service as $info)
        {!!html_entity_decode($info->details)!!}
      @endforeach
      @endif
    </div>


</div>
</div>





<div class="bot1_wrapper">
<div class="container">
  <div class="row">
    <div class="col-sm-3">
      <div class="logo2_wrapper">
        <a href="{{ URL::to('/') }}/" class="logo">
          <img src="{{ URL::to('/') }}/images/logo.png" alt="" class="img-responsive">
          <img src="{{ URL::to('/') }}/images/190x180/iso.png" alt="" class="img-responsive">
        </a>
      </div>
    </div>
    <div class="col-sm-3">
      <div class="bot1_title">Our Address</div>
      <div style="line-height:25px">
          Address : MED LAB DIAGNOSTICS PRIVATE LIMITED HEAD OFFICE<br>
          H.DHUBUGASDOSUGE <br>
          KURAGI GOLHI, HENVEIRU<br>
          PO BOX 200026, <br>
          MALE’ REPUBLIC OF MALDIVES
      </div>
    </div>
    <div class="col-sm-3">
      <div class="bot1_title">Our Contact</div>
      <div style="line-height:25px">
        Phone : + (960) 3010878<br>
        Fax : + (960) 3010877 <br>
        Email : ops@aviamaldives.com<br>
        ISO Certificatio No. : 32665-A01<br>
      </div>
    </div>
    <div class="col-sm-3">
      <div class="bot1_title">Social Media</div>
      <div>
        <ul class="social clearfix">
          <li><a style="font-size:35px; color:#3b5998" href="https://www.facebook.com/pages/Medlab-Diagnostics-Pvt-Ltd/705679799583849" target="_blank"><i class="fa fa-facebook"></i></a></li>
          <li><a style="font-size:35px; color:#29a2f1"href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a></li>
          <li><a style="font-size:35px; color:#b83289"href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
</div>

<div class="bot2_wrapper">
<div class="container">
  <div class="left_side">
    Copyright &copy; <?=date("Y");?> <strong>MedLab Diagnostics Pvt Ltd</strong>   <span>|</span>   <a href="{{ URL::to('/') }}/about">About Us</a>   <span>
  </div>
</div>
</div>
</div>
@stop
