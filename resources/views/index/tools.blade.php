@extends('masterlayout')

@section('spc_header')
    <meta name="description" content="index desc"/>
    <meta name="keywords" content="index keywords"/>
@stop

@section('styles_header')
  <style media="screen">
  .popover {
    background-color:#252525;

  }
  .popover.bottom .arrow:after {
      border-bottom-color: #252525;
  }
  .popover.top {
      border-bottom-color: #252525;
  }
  </style>
@stop


@section('spc_footer')
  <script type="text/javascript">
    $(document).ready(function(){

        $("#insta").css("line-height","0").css("-webkit-column-count", "5").css("-webkit-column-gap","0px").css("-moz-column-count","5").css("-moz-column-gap","0px").css("column-count","5").css("column-gap","0px");
        $.ajax({url: "{{ URL::to('/') }}/getinstagramfeeds", success: function(result){
              $("#insta").html(result + "<br>");
        }});

        $("#twitter").css("-webkit-column-count", "5").css("-webkit-column-gap","0px").css("-moz-column-count","5").css("-moz-column-gap","0px").css("column-count","5").css("column-gap","0px");
        $.ajax({url: "{{ URL::to('/') }}/gettwitterfeeds", success: function(result){
              $("#twitter").html(result + "<br>");
        }});


    })
  </script>
@stop

@section('topnavi')
  @include('contact.topnavi')
@stop


@section('content')
<div id="main">
<div class="top1_wrapper">
  <div class="container">
    <div class="top1 clearfix">
      <div class="email1"><a href="#">ops@aviamaldives.com</a></div>
      <div class="phone1">+(960) 778 6419</div>
      <div class="social_wrapper">
        <ul class="social clearfix">
          <li><a href="#"><i class="fa fa-facebook"></i></a></li>
          <li><a href="#"><i class="fa fa-twitter"></i></a></li>
          <li><a href="#"><i class="fa fa-skype"></i></a></li>
          <li><a href="#"><i class="fa fa-instagram"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>

@yield('topnavi')





<div id="why1">
  <div class="container">

    <h2 class="animated" style="float:left">
      Tools
    </h2>

    <div style="clear:both">
      <h4>Handful of tools for your flight planning.</h4>
      &bull; <a href="{{ URL::to('/') }}/notam">NOTAM</a><br>
      &bull; <a href="{{ URL::to('/') }}/weather">Weather Information</a><br>
      &bull; <a href="{{ URL::to('/') }}/aip">Aeronautical Information Publication (AIP)</a><br>
    </div>


</div>
</div>





<div class="bot1_wrapper">
  <div class="container">
    <div class="row">
      <div class="col-sm-3">
        <div class="logo2_wrapper">
          <a href="{{ URL::to('/') }}/" class="logo">
            <img src="{{ URL::to('/') }}/images/logo.png" alt="" class="img-responsive">
          </a>
        </div>
        <ul id="company_contact">
          <li>Phone : + (960) 300 0058</li>
          <li>Fax : + (960) 300 0057 </li>
          <li>Email : ops@aviamaldives.com</li>
          <li>SITA : MLEAVXH</li>
          <li>ARINC : MLEAMXH</li>
          <li>AFTN : KMYVXAAA</li>

        </ul>
      </div>
      <div class="col-sm-3">
        <div class="bot1_title">Our Instagram</div>
        <div id="insta" align="center">
          <div class='uil-ring-css' style='transform:scale(0.7);'><div></div></div>
        </div>

      </div>
      <div class="col-sm-3">
        <div class="bot1_title">Our Twitter</div>
          <div id="twitter" align="left" style="height:auto;display:table;">
            <div class='uil-ring-css' style='transform:scale(0.7);'><div></div></div>
          </div>
      </div>
      <div class="col-sm-3">
        <div class="bot1_title">Newsletter</div>
        <div class="newsletter_block">
          <div class="newsletter-wrapper clearfix">
          <form class="newsletter" action="javascript:;">
            <input type="text" name="s" value='Email Address' onBlur="if(this.value=='') this.value='Email Address'" onFocus="if(this.value =='Email Address' ) this.value=''">
            <button class="btn-default btn3">SUBMIT</button>
          </form>
        </div>
        </div>
        <div class="phone2">+(960) 778 6419</div>
        <div class="support1"><a href="#">ops@aviamaldives.com</a></div>
      </div>
    </div>
  </div>
</div>

<div class="bot2_wrapper">
  <div class="container">
    <div class="left_side">
      Copyright &copy; 2017 <strong>AVIA MALDIVES PVT LTD</strong>   <span>|</span>   <a href="#">About Us</a>   <span>
    </div>
  </div>
</div>
</div>
@stop
