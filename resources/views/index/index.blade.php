@extends('masterlayout')

@section('spc_header')
  <meta name="description" content="Medlab Diagnostics Private Limited formerly known as Medlab Diagnostics is formed for a specialty for medical devices and lab consumables and Chemicals to sell products to companies and market and introduce latest medical devices, related any surgical niche market within Maldives. "/>
  <meta name="keywords" content="Medlab, Diagnostics, Maldives"/>
@stop

@section('styles_header')
  <style media="screen">
  .bg1 { background-image: url("{{ URL::to('/') }}/library/backgrounds/1.jpg"); }
  .bg2 { background-image: url("{{ URL::to('/') }}/library/inners/statsinner.jpg"); }
  /* Large desktop */
  @media (min-width: 2560px) {
  #slider_wrapper{background: url('{{ URL::to('/') }}/library/banners/<?=rand(1,2);?>.jpg') top center no-repeat; background-size: cover; padding-bottom: 300px;
      padding-top: 370px; }
  }
  @media (min-width: 1200px) {

  #slider_wrapper{background: url('{{ URL::to('/') }}/library/banners/<?=rand(1,2);?>.jpg') top center no-repeat; background-size: cover; padding-bottom: 300px;
        padding-top: 370px; }
  }
  /* Default landscape and desktop to large desktop */
  @media (max-width: 1199px) {
  #slider_wrapper{background: url('{{ URL::to('/') }}/library/banners/<?=rand(1,2);?>.jpg') top center no-repeat; background-size: cover;
        padding-top: 370px; }
  }
  @media (min-width: 992px) and (max-width: 1199px) { #happy1 .img1{margin-top: 220px;}
    .form2_flights .select1_inner{width: 55px;}
    .form3 .sel{width: 90px;}
  }
  .popover {
    background-color:#252525;

  }
  .popover.bottom .arrow:after {
      border-bottom-color: #252525;
  }
  .popover.top {
      border-bottom-color: #252525;
  }
  </style>
@stop


@section('spc_footer')
<script>
  $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();
  });
</script>
@stop

@section('topnavi')
  @include('index.topnavi')
@stop


@section('content')
<div id="main">
<div class="top1_wrapper">
  <div class="container">
    <div class="top1 clearfix">
      <div class="email1"><a href="#">info@medlabdiognostics.com</a></div>
      <div class="phone1">+(960) 3010878</div>
      <div class="social_wrapper">
        <ul class="social clearfix">
          <li><a href="https://www.facebook.com/pages/Medlab-Diagnostics-Pvt-Ltd/705679799583849" target="_blank"><i class="fa fa-facebook"></i></a></li>
          <li><a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a></li>
          <li><a href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>

@yield('topnavi')

<div id="slider_wrapper">
  <div class="container">
    <div id="slider_inner">
      <div class="">
        <div id="slider">

          <div class="">
            <div class="carousel-box">
              <div class="inner">
                <div class="carousel main">
                  <ul>
                    <li>
                      <div class="slider">
                        <div class="slider_inner">
                          <div class="txt2"><span>OUR OBJECTIVE</span></div>
                          <div class="txt3"><span>Our objective is to provide consistent, high quality and value for money service in whatever we do, ensuring highest satisfaction to our esteemed customers and our agents.  The company's success has been achieved through hard work, innovation and commitment for excellence.</span></div>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="slider">
                        <div class="slider_inner">
                          <div class="txt2"><span>OUR VISION</span></div>
                          <div class="txt3"><span>Our vision is clear: Med Lab Diagnostics is dedicated to providing our customers with world class products and services. This means that from initial contact through service delivery, our team will work fulltime to provide the outstanding quality and service that our customer needs to get their service done on time and on budget.</span></div>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div class="slider">
                        <div class="slider_inner">
                          <div class="txt2"><span>OUR MISSION</span></div>
                          <div class="txt3"><span>We aspire to be the most admirable and valuable company in the health care market. Our goal is to enrich our customer needs bringing out exciting healthcare solutions.</span></div>
                        </div>
                      </div>
                    </li>

                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="slider_pagination"></div>
        </div>
      </div>
    </div>
  </div>
</div>



<div id="why1">
  <div class="container">

    <h2 class="animated">WHO WE ARE</h2>

    <div class="title1 animated" style="width:auto"> Medlab Diagnostics Private Limited formerly known as Medlab Diagnostics is formed for a specialty for medical devices and lab consumables & Chemicals to sell products to companies and market and introduce latest medical devices, related any surgical niche market within Maldives.
    <br><a href="{{ URL::to('/') }}/about">[Learn More]</a></div>
    <br><br>

  </div>
</div>

<div id="parallax1" class="parallax">
  <div class="bg1 parallax-bg"></div>
  <div class="overlay"></div>
  <div class="parallax-content">
    <div class="container">

      <div class="row">
        <div class="col-sm-12 animated">

          @foreach($data_vrmm as $info)
          <div class="txt1">" {!!html_entity_decode($info->details)!!} "</div>
          <div class="txt3">-{{ $info->customer }}</div>
          @endforeach

        </div>
      </div>

    </div>
  </div>
</div>

<div id="popular_cruises1">
  <div class="container">

    <h2 class="animated">OUR SERVICES</h2>

    <div class="title1 animated" style="width:auto">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</div>

    <br><br>

    <div id="popular_wrapper" class="animated" data-animation="fadeIn" data-animation-delay="300" style=" clear:both;">
      <div id="popular_inner">
        <div class="">
          <div id="popular">
            <div class="">
              <div class="carousel-box">
                <div class="inner">
                  <div class="carousel main">
                    <ul>
                      <li>
                        <div class="popular">
                          <div class="popular_inner">
                            <figure>
                              <img src="{{ URL::to('/') }}/library/inners/labservices.jpg" alt="" class="img-responsive">
                            </figure>
                            <div class="caption">
                              <div class="txt1"><span>Laboratory Services</span></div>
                              <div class="txt2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
                              <div class="txt3 clearfix">
                                <div class="right_side"><a href="http://medlabdiagnostics.com/services/laboratary" class="btn-default btn1">More</a></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li><div class="popular">
                        <div class="popular_inner">
                          <figure>
                            <img src="{{ URL::to('/') }}/library/inners/clinicservices.jpg" alt="" class="img-responsive">
                          </figure>
                          <div class="caption">
                            <div class="txt1"><span>Advance Clinic Lab Facilities</span></div>
                            <div class="txt2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
                            <div class="txt3 clearfix">
                              <div class="right_side"><a href="http://medlabdiagnostics.com/services/cliniclabfacilities" class="btn-default btn1">More</a></div>
                            </div>
                          </div>
                        </div>
                      </div>
                      </li>
                      <li>
                        <div class="popular">
                          <div class="popular_inner">
                            <figure>
                              <img src="{{ URL::to('/') }}/library/inners/wholesale.jpg" alt="" class="img-responsive">
                            </figure>
                            <div class="caption">
                              <div class="txt1"><span>Wholesale & Supply</span></div>
                              <div class="txt2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
                              <div class="txt3 clearfix">
                                <div class="right_side"><a href="http://medlabdiagnostics.com/services/wholesalesupply" class="btn-default btn1">More</a></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="popular">
                          <div class="popular_inner">
                            <figure>
                              <img src="{{ URL::to('/') }}/library/inners/corporate.jpg" alt="" class="img-responsive">
                            </figure>
                            <div class="caption">
                              <div class="txt1"><span>Corporate Responsibility</span></div>
                              <div class="txt2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
                              <div class="txt3 clearfix">
                                <div class="right_side"><a href="http://medlabdiagnostics.com/services/corporate" class="btn-default btn1">More</a></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="popular_pagination"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div id="parallax3" class="parallax">
  <div class="bg2 parallax-bg"></div>
  <div class="overlay"></div>
  <div class="parallax-content">
    <div class="container">

      <div class="row">
        <div class="col-sm-12 col-md-12">
          <div class="content" style="background-color:rgba(6, 39, 60, 0.77); padding:2em">
            <div class="section-index">
            <div class="container">
              <div class="content">
                <div class="row text-center">
                  <div class="col-xs-6 col-sm-3 mb-sm-40">
                    <div class="timer">
                      <div class="timer__count txt1" data-to="2" data-speed="2000">2</div>
                      <div class="timer__text">Advance Labs</div>
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-3 mb-sm-40">
                    <div class="timer">
                      <div class="timer__count txt1" data-to="3" data-speed="2000">3</div>
                      <div class="timer__text">Pharmacies</div>
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-3">
                    <div class="timer">
                      <div class="timer__count txt1" data-to="51" data-speed="2000">51</div>
                      <div class="timer__text">Local Staff</div>
                    </div>
                  </div>
                  <div class="col-xs-6 col-sm-3">
                    <div class="timer">
                      <div class="timer__count txt1" data-to="11" data-speed="2000">11</div>
                      <div class="timer__text">Foreign Staff</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div id="popular_cruises1" style="background-color:#EEE">
  <div class="container">
    <h2 class="animated">NEWS</h2>
    <br><br>
    <div id="popular_wrapper" class="animated" data-animation="fadeIn" data-animation-delay="300">
      <div id="popular_inner">
        <div class="row">
          @foreach($data_news as $details)
          <div class="col-md-3">
            <div class="popular">
                <div class="popular_inner">
                  <figure>
                    <img src="{{ URL::to('/') }}/library/news/200x100/{{ $details->img }}" alt="" class="img-responsive">
                  </figure>
                  <div class="caption" style="min-height:250px">
                    <div class="txt1"><span>{{ $details->title }}</span></div>
                    <div class="txt2"><strong>{{ date('d M Y', strtotime($details->entrydate)) }} </strong>- {{ $details->description }}</div>
                    <div class="txt3 clearfix">
                      <div class="right_side"><a href="{{ URL::to('/') }}/news/details/{{ $details->incre }}" class="btn-default btn1">Details</a></div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>



<div id="partners" >
  <div class="container">
    <div class="row">
      @foreach($data_brands as $details)
      <div class="col-sm-4 col-md-3 col-xs-6">
        <div class="thumb1 animated">
          <div class="thumbnail clearfix">
            <a href="{{ $details->url }}" target="_blank"  data-html="true"  data-toggle="tooltip" title="{{ $details->title }} <br> visit : {{ $details->url }} ">
              <figure>
                <img src="{{ URL::to('/') }}/library/partners/200x100/{{ $details->img }}" alt="" class="img1 img-responsive">
                <img src="{{ URL::to('/') }}/library/partners/200x100/{{ $details->img }}" alt="" class="img2 img-responsive">
              </figure>
            </a>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</div>

<div class="bot1_wrapper">
  <div class="container">
    <div class="row">
      <div class="col-sm-3">
        <div class="logo2_wrapper">
          <a href="{{ URL::to('/') }}/" class="logo">
            <img src="{{ URL::to('/') }}/images/logo.png" alt="" class="img-responsive">
            <img src="{{ URL::to('/') }}/images/190x180/iso.png" alt="" class="img-responsive">
          </a>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="bot1_title">Our Address</div>
        <div style="line-height:25px">
            Address : MED LAB DIAGNOSTICS PRIVATE LIMITED HEAD OFFICE<br>
            H.DHUBUGASDOSUGE <br>
            KURAGI GOLHI, HENVEIRU<br>
            PO BOX 200026, <br>
            MALE’ REPUBLIC OF MALDIVES
        </div>
      </div>
      <div class="col-sm-3">
        <div class="bot1_title">Our Contact</div>
        <div style="line-height:25px">
          Phone : + (960) 3010878<br>
          Fax : + (960) 3010877 <br>
          Email : ops@aviamaldives.com<br>
          ISO Certificatio No. : 32665-A01<br>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="bot1_title">Social Media</div>
        <div>
          <ul class="social clearfix">
            <li><a style="font-size:35px; color:#3b5998" href="https://www.facebook.com/pages/Medlab-Diagnostics-Pvt-Ltd/705679799583849" target="_blank"><i class="fa fa-facebook"></i></a></li>
            <li><a style="font-size:35px; color:#29a2f1"href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a></li>
            <li><a style="font-size:35px; color:#b83289"href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="bot2_wrapper">
  <div class="container">
    <div class="left_side">
      Copyright &copy; <?=date("Y");?> <strong>MedLab Diagnostics Pvt Ltd</strong>   <span>|</span>   <a href="{{ URL::to('/') }}/about">About Us</a>   <span>
    </div>
  </div>
</div>
</div>
@stop
