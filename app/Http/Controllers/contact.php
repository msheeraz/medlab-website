<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class contact extends Controller
{
  public function index() {
    $data_contact = DB::table('tbl_contact')->orderBy('incre', '1')->get();
    return view('contact.index',compact('data_contact'));
  }
}
