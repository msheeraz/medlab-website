<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class careers extends Controller
{
  public function index() {
    $data_careers = DB::table('tbl_careers')->orderBy('incre', '1')->get();
    return view('careers.index',compact('data_careers'));
  }
}
