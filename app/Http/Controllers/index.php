<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;


class Index extends Controller
{
    public function index() {

      $data_vrmm = DB::table('tbl_testimonials')->get()->random(1);
      $data_news= DB::table('tbl_news')->orderBy('created_at')->simplePaginate(4);
      $data_brands= DB::table('tbl_brands')->orderBy('created_at')->get();
      return view('index.index',compact('data_vrmm','data_news','data_brands'));
    }
    public function tools() {
      return view('index.tools');
    }




}
