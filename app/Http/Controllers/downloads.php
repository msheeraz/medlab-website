<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class downloads extends Controller
{
  public function index() {
    $data_downloads = DB::table('tbl_downloads')->orderBy('title', 'ASC')->get();
    return view('downloads.index',compact('data_downloads'));
  }

}
