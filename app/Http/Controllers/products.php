<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class products extends Controller
{
  public function index() {
    $data_products = DB::table('tbl_products')->orderBy('incre', '1')->simplePaginate(20);
    return view('products.index',compact('data_products'));
  }
  public function productsDetails($id) {
    $data_products = DB::table('tbl_products')->where('incre', $id)->get();
    return view('products.details',compact('data_products'));
  }
}
