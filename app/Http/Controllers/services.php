<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class services extends Controller
{
  public function index($handler) {
    $data_service = DB::table('tbl_services')->where('slug','=',$handler)->get();
    return view('services.index',compact('data_service'));
  }
}
