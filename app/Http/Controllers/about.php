<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class about extends Controller
{
  public function index() {
    $data_about = DB::table('tbl_about')->orderBy('incre', '1')->get();
    return view('about.index',compact('data_about'));
  }
}
