<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class news extends Controller
{
  public function index() {
    $data_news = DB::table('tbl_news')->orderBy('incre', '1')->simplePaginate(20);
    return view('news.index',compact('data_news'));
  }
  public function newsDetails($id) {
    $data_news = DB::table('tbl_news')->where('incre', $id)->get();
    return view('news.details',compact('data_news'));
  }
}
