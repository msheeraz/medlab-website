<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('');
// });

Route::get('/', 'index@index');
Route::get('/news',  'news@index');
Route::get('/news/details/{id}',  'news@newsDetails');
Route::get('/services/{handler}',  'services@index');
Route::get('/products',  'products@index');
Route::get('/products/details/{id}',  'products@productsDetails');
Route::get('/downloads',  'downloads@index');
Route::get('/careers',  'careers@index');
Route::get('/about',  'about@index');
Route::get('/contact',  'contact@index');


Route::get('/library/partners/{size}/{name}', function($size = NULL, $name = NULL){
    if(!is_null($size) && !is_null($name)){
        $size = explode('x', $size);
        $cache_image = Image::cache(function($image) use($size, $name){
           return $image->make(url('/library/partners/'.$name))->resize($size[0], $size[1]);
        }, 10); // cache for 10 minutes

        return Response::make($cache_image, 200, ['Content-Type' => 'image']);
    } else {
        abort(404);
    }
});
Route::get('/images/{size}/{name}', function($size = NULL, $name = NULL){
    if(!is_null($size) && !is_null($name)){
        $size = explode('x', $size);
        $cache_image = Image::cache(function($image) use($size, $name){
           return $image->make(url('/images/'.$name))->resize($size[0], $size[1]);
        }, 10); // cache for 10 minutes

        return Response::make($cache_image, 200, ['Content-Type' => 'image']);
    } else {
        abort(404);
    }
});
Route::get('/library/news/{size}/{name}', function($size = NULL, $name = NULL){
    if(!is_null($size) && !is_null($name)){
        $size = explode('x', $size);
        $cache_image = Image::cache(function($image) use($size, $name){
           return $image->make(url('/library/news/'.$name))->resize($size[0], $size[1]);
        }, 10); // cache for 10 minutes

        return Response::make($cache_image, 200, ['Content-Type' => 'image']);
    } else {
        abort(404);
    }
});
Route::get('/library/products/{size}/{name}', function($size = NULL, $name = NULL){
    if(!is_null($size) && !is_null($name)){
        $size = explode('x', $size);
        $cache_image = Image::cache(function($image) use($size, $name){
           return $image->make(url('/library/products/'.$name))->resize($size[0], $size[1]);
        }, 10); // cache for 10 minutes

        return Response::make($cache_image, 200, ['Content-Type' => 'image']);
    } else {
        abort(404);
    }
});


