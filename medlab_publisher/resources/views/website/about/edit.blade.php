@extends('masterlayout')

@section('sidenavi')
  @include('website.sidenavi')
@endsection

@section('spc_header')
    <link href="{{ URL::to('/') }}/public/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::to('/') }}/public/assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
    <script src="{{ URL::to('/') }}/public/assets/plugins/summernote-master/summernote.js"></script>
    <script src="{{ URL::to('/') }}/public/assets/js/pages/form-elements.js"></script>
    <script src="{{ URL::to('/') }}/public/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="{{ URL::to('/') }}/public/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
@endsection

@section('spc_footer')
  <script type="text/javascript">

            $("#lipabout").addClass("active");

            $('.date-picker').datepicker({
                orientation: "top auto",
                autoclose: true,
            });
            $('.datetimepicker').datetimepicker();
            setTimeout(function(){ $(".alert").fadeOut("slow"); },3000);


            $('#details').summernote({
             fontNames: ['DEArial', 'Arial Black', 'Comic Sans MS', 'Courier New', 'Merriweather'],
             fontNamesIgnoreCheck: ['Merriweather']
            });

  </script>
@endsection

@section('content')

        <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ URL::to('/') }}/about/edit/@foreach($details as $detail){{ $detail->incre }}@endforeach">
        <main class="page-content content-wrap">
            <div class="navbar">
                <div class="navbar-inner">
                    <div class="sidebar-pusher">
                        <a href="javascript:void(0);" class="waves-effect waves-button waves-classic push-sidebar">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                    <div class="logo-box">
                        <a href="{{ URL::to('/') }}" class="logo-text">Publisher</a>
                    </div><!-- Logo Box -->
                    <div class="topmenu-outer">
                        <div class="top-menu">
                            <ul class="nav navbar-nav navbar-right">
                                    <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                                Logout
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                            </ul><!-- Nav -->
                        </div><!-- Top Menu -->
                    </div>
                </div>
            </div><!-- Navbar -->
            <div class="page-sidebar sidebar">
                <div class="page-sidebar-inner slimscroll">
                    <ul class="menu accordion-menu" style="overflow: hidden;">
                        @yield('sidenavi')
                    </ul>
                </div><!-- Page Sidebar Inner -->
            </div><!-- Page Sidebar -->
            <div class="page-inner">
                <div id="main-wrapper">
                    <div class="row m-t-md">
                        <div class="col-md-12">
                            <h2>About</h2>
                            <ol class="breadcrumb">
                                <li><a href="{{ URL::to('/') }}/">Home</a></li>
                                <li><a href="{{ URL::to('/') }}/about">About</a></li>
                            </ol>
                            <div class="mailbox-content">
                             <div class="panel panel-white">

                             <div class="error">
                                @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <strong>Whoops!</strong><br>There were some problems with your input.<br><br>
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                 @else
                                    @if (Request::isMethod('post'))
                                    <div class="alert alert-success">
                                        <strong>Success</strong><br>Changes Completed<br><br>
                                    </div>
                                    @endif
                                @endif
                               </div>


                                   <div class="panel-body">
                                           <div class="form-group">
                                               <label for="title">Airport</label>
                                               <input type="text" class="form-control" id="title" readonly="readonly" required="required" name="title" placeholder="Enter Title" value="@foreach($details as $detail){{ $detail->title }}@endforeach">
                                           </div>

                                           <div class="form-group">
                                               <label for="department">Details</label>
                                               <textarea id="details" class="summernote" name="details">@foreach($details as $detail){{ $detail->details }}@endforeach</textarea>
                                           </div>
                                           <a href="{{ URL::to('/') }}/aip"><button type="button" class="btn btn-danger pull-right" style="margin-left: 10px;">CANCEL</button></a>
                                           <button type="submit" class="btn btn-primary pull-right">UPDATE</button>
                                   </div>
                            </div>

                            {{ csrf_field() }}

                            </div>
                        </div>
                    </div><!-- Row -->
                </div><!-- Main Wrapper -->
                <div class="page-footer">
                    <p class="no-s"><?=date("Y");?> &copy; Mohamed Sheeraz.</p>
                </div>
            </div><!-- Page Inner -->
        </main><!-- Page Content -->
        </form>
@endsection
