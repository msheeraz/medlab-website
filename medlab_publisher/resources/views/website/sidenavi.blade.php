@section('sidenavi')
        <li data-toggle="tooltip" data-placement="right" title="Services" id="liservices"><a href="{{ URL::to('/') }}/services" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-plane"></span><p>Services</p></a></li>
        <li data-toggle="tooltip" data-placement="right" title="Downloads" id="lidownloads"><a href="{{ URL::to('/') }}/downloads" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-download-alt"></span><p>Downloads</p></a></li>
        <li data-toggle="tooltip" data-placement="right" title="Testimonials" id="litestimonials"><a href="{{ URL::to('/') }}/testimonials" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-comment"></span><p>Testimonials</p></a></li>
        <li data-toggle="tooltip" data-placement="right" title="Products" id="liproducts" ><a href="{{ URL::to('/') }}/products" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-tree-conifer"></span><p>Products</p></a></li>
        <li data-toggle="tooltip" data-placement="right" title="Brands" id="librands" ><a href="{{ URL::to('/') }}/brands" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-heart"></span><p>Brands</p></a></li>
        <li data-toggle="tooltip" data-placement="right" title="News" id="linews" ><a href="{{ URL::to('/') }}/news" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-list-alt"></span><p>News</p></a></li>
        <li data-toggle="tooltip" data-placement="right" title="Careers" id="licareers" ><a href="{{ URL::to('/') }}/careers" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-briefcase"></span><p>Careers</p></a></li>
        <li data-toggle="tooltip" data-placement="right" title="About" id="lipabout" ><a href="{{ URL::to('/') }}/about" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-map-marker"></span><p>About Us</p></a></li>
        <li data-toggle="tooltip" data-placement="right" title="Contact" id="lipcontact" ><a href="{{ URL::to('/') }}/contact" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-phone-alt"></span><p>Contact Us</p></a></li>
    </ul>
@endsection
