@extends('masterlayout')

@section('sidenavi')
  @include('website.sidenavi')
@endsection

@section('spc_header')
    <link href="{{ URL::to('/') }}/public/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::to('/') }}/public/assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
    <script src="{{ URL::to('/') }}/public/assets/plugins/summernote-master/summernote.js"></script>
    <script src="{{ URL::to('/') }}/public/assets/js/pages/form-elements.js"></script>
    <script src="{{ URL::to('/') }}/public/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="{{ URL::to('/') }}/public/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
@endsection

@section('spc_footer')
  <script type="text/javascript">

            $("#litenders").addClass("active");

            $('.date-picker').datepicker({
                orientation: "top auto",
                autoclose: true,
            });
            $('.datetimepicker').datetimepicker();
            setTimeout(function(){ $(".alert").fadeOut("slow"); },3000);


            $('#details').summernote({
             fontNames: ['DEArial', 'Arial Black', 'Comic Sans MS', 'Courier New', 'Merriweather'],
             fontNamesIgnoreCheck: ['Merriweather']
            });

  </script>
@endsection

@section('content')

        <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ URL::to('/') }}/tenders/new">
        <main class="page-content content-wrap">
            <div class="navbar">
                <div class="navbar-inner">
                    <div class="sidebar-pusher">
                        <a href="javascript:void(0);" class="waves-effect waves-button waves-classic push-sidebar">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                    <div class="logo-box">
                        <a href="{{ URL::to('/') }}" class="logo-text">Publisher</a>
                    </div><!-- Logo Box -->
                    <div class="topmenu-outer">
                        <div class="top-menu">
                            <ul class="nav navbar-nav navbar-right">
                                 <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                                Logout
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                            </ul><!-- Nav -->
                        </div><!-- Top Menu -->
                    </div>
                </div>
            </div><!-- Navbar -->
            <div class="page-sidebar sidebar">
                <div class="page-sidebar-inner slimscroll">
                    <ul class="menu accordion-menu" style="overflow: hidden;">
                        @yield('sidenavi')
                    </ul>
                </div><!-- Page Sidebar Inner -->
            </div><!-- Page Sidebar -->
            <div class="page-inner">
                <div id="main-wrapper">
                    <div class="row m-t-md">
                        <div class="col-md-12">
                            <h2>Tenders</h2>
                            <ol class="breadcrumb">
                                <li><a href="{{ URL::to('/') }}/">Home</a></li>
                                <li><a href="{{ URL::to('/') }}/tenders">Tenders</a></li>
                            </ol>
                            <div class="mailbox-content">
                             <div class="panel panel-white">

                             <div class="error">
                                @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <strong>Whoops!</strong><br>There were some problems with your input.<br><br>
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                 @else
                                    @if (Request::isMethod('post'))
                                    <div class="alert alert-success">
                                        <strong>Success</strong><br>Changes Completed<br><br>
                                    </div>
                                    @endif
                                @endif
                               </div>

                                <div class="panel-body">
                                        <div class="form-group">
                                            <label for="title">Title</label>
                                            <input type="text" class="form-control" id="title" required="required" name="title" placeholder="Enter Title">
                                        </div>
                                         <div class="form-group">
                                            <label for="department">Department</label>
                                            <input type="text" class="form-control" required="required" name="department" id="department" placeholder="Enter Department" >
                                        </div>
                                        <div class="form-group">
                                            <label for="reference_no">Reference No.</label>
                                            <input type="text" class="form-control" required="required" id="reference_no" name="reference_no" placeholder="Enter Reference">
                                        </div>
                                        <div class="form-group">
                                            <label for="publishDate">Publish Date</label>
                                            <input type="text" class="form-control date-picker" data-date-format='yyyy-mm-dd' name="publishDate" required="required" id="publishDate" placeholder="Enter Publish Date (YYYY-MM-DD)">
                                        </div>
                                         <div class="form-group">
                                            <label for="publishDateTime">Due DateTime</label>
                                            <input type="text" class="form-control datetimepicker"  required="required" name="publishDateTime" id="publishDateTime" placeholder="Enter Due Date and Time (YYYY-MM-DD HH:MM)">
                                        </div>
                                        <div class="form-group">
                                            <label for="attachement">Attachement (PDF)</label>
                                            <input id="attachement" name="attachement" type="file" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="department">Details</label>
                                            <textarea id="details" class="summernote" name="details"></textarea>
                                        </div>

                                        <a href="{{ URL::to('/') }}/tenders"><button type="button" class="btn btn-danger pull-right" style="margin-left: 10px;">CANCEL</button></a>
                                        <button type="submit" class="btn btn-primary pull-right">ADD</button>
                                </div>
                            </div>

                            {{ csrf_field() }}

                            </div>
                        </div>
                    </div><!-- Row -->
                </div><!-- Main Wrapper -->
                <div class="page-footer">
                    <p class="no-s"><?=date("Y");?> &copy; Mohamed Sheeraz.</p>
                </div>
            </div><!-- Page Inner -->
        </main><!-- Page Content -->
        </form>
@endsection
