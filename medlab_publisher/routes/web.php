<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'indexController@index')->middleware('auth');
Route::get('/services', 'indexController@services')->middleware('auth');;
Route::get('/services/new', 'indexController@servicesnew')->middleware('auth');;
Route::get('/services/edit/{id}', 'indexController@servicesedit')->middleware('auth');;
Route::post('/services/edit/{id}', 'indexController@servicesupdate')->middleware('auth');;
Route::get('/services/delete/{id}', 'indexController@servicesdelete')->middleware('auth');;
Route::post('/services/new', 'indexController@servicesadd')->middleware('auth');;



Route::get('/downloads', 'indexController@downloads')->middleware('auth');;
Route::get('/downloads/new', 'indexController@downloadsnew')->middleware('auth');;
Route::post('/downloads/new', 'indexController@downloadsadd')->middleware('auth');;
Route::get('/downloads/edit/{id}', 'indexController@downloadsedit')->middleware('auth');;
Route::post('/downloads/edit/{id}', 'indexController@downloadsupdate')->middleware('auth');
Route::get('/downloads/delete/{id}', 'indexController@downloadsdelete')->middleware('auth');;


Route::get('/testimonials', 'indexController@testimonials')->middleware('auth');;
Route::get('/testimonials/new', 'indexController@testimonialsnew')->middleware('auth');;
Route::post('/testimonials/new', 'indexController@testimonialsadd')->middleware('auth');;
Route::get('/testimonials/delete/{id}', 'indexController@testimonialsdelete')->middleware('auth');;
Route::get('/testimonials/edit/{id}', 'indexController@testimonialsedit')->middleware('auth');;
Route::post('/testimonials/edit/{id}', 'indexController@testimonialsupdate')->middleware('auth');;



Route::get('/about', 'indexController@about')->middleware('auth');;
Route::get('/about/edit/{id}', 'indexController@aboutedit')->middleware('auth');;
Route::post('/about/edit/{id}', 'indexController@aboutupdate')->middleware('auth');;

Route::get('/contact', 'indexController@contact')->middleware('auth');;
Route::get('/contact/edit/{id}', 'indexController@contactedit')->middleware('auth');;
Route::post('/contact/edit/{id}', 'indexController@contactupdate')->middleware('auth');;



Route::get('/products', 'indexController@products')->middleware('auth');;
Route::get('/products/new', 'indexController@productsnew')->middleware('auth');;
Route::post('/products/new', 'indexController@productsadd')->middleware('auth');;
Route::get('/products/edit/{id}', 'indexController@productsedit')->middleware('auth');;
Route::post('/products/edit/{id}', 'indexController@productsupdate')->middleware('auth');;
Route::get('/products/delete/{id}', 'indexController@productsdelete')->middleware('auth');;


Route::get('/news', 'indexController@news')->middleware('auth');
Route::get('/news/new', 'indexController@newsnew')->middleware('auth');
Route::post('/news/new', 'indexController@newsadd')->middleware('auth');
Route::get('/news/edit/{id}', 'indexController@newsedit')->middleware('auth');
Route::post('/news/edit/{id}', 'indexController@newsupdate')->middleware('auth');
Route::get('/news/delete/{id}', 'indexController@newsdelete')->middleware('auth');


Route::get('/brands', 'indexController@brands')->middleware('auth');
Route::get('/brands/new', 'indexController@brandsnew')->middleware('auth');
Route::post('/brands/new', 'indexController@brandsadd')->middleware('auth');
Route::get('/brands/delete/{id}', 'indexController@brandsdelete')->middleware('auth');



Route::get('/careers', 'indexController@careers')->middleware('auth');;
Route::get('/careers/edit/{id}', 'indexController@careersedit')->middleware('auth');;
Route::post('/careers/edit/{id}', 'indexController@careersupdate')->middleware('auth');;



Auth::routes();
Route::get('/home', 'HomeController@index')->middleware('auth');
