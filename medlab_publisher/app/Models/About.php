<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
  protected $primaryKey = 'incre';
  protected $table = 'tbl_about';

  protected $fillable = [
    'title',
    'details',
  ];
}
