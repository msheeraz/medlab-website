<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brands extends Model
{
  protected $primaryKey = 'incre';
  protected $table = 'tbl_brands';

  protected $fillable = [
    'title',
    'url',
  ];
}
