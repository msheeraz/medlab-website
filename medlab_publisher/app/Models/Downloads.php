<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Downloads extends Model
{
  protected $primaryKey = 'incre';
  protected $table = 'tbl_downloads';

  protected $fillable = [
    'title',
    'file',
  ];
}
