<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Careers extends Model
{
  protected $primaryKey = 'incre';
  protected $table = 'tbl_careers';

  protected $fillable = [
    'title',
    'details',
  ];
}
