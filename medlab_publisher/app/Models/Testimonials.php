<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Testimonials extends Model
{
  protected $primaryKey = 'incre';
  protected $table = 'tbl_testimonials';

  protected $fillable = [
    'customer',
    'details',
  ];
}
