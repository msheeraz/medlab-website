<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
  protected $primaryKey = 'incre';
  protected $table = 'tbl_news';

  protected $fillable = [
    'title',
    'details',
    'description',
    'entrydate',
  ];
}
