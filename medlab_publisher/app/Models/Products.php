<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
  protected $primaryKey = 'incre';
  protected $table = 'tbl_products';

  protected $fillable = [
    'title',
    'details',
    'description',
  ];
}
