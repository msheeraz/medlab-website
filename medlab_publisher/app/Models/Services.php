<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    protected $primaryKey = 'incre';
    protected $table = 'tbl_services';

    protected $fillable = [
    	'title',
    	'details',
    ];
}
