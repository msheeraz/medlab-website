<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Models\Services;
use App\Models\Downloads;
use App\Models\Testimonials;
use App\Models\Products;
use App\Models\News;
use App\Models\Brands;
use App\Models\Careers;
use App\Models\About;
use App\Models\Contact;
use DB;
use Session;


class indexController extends Controller
{
    public function index() {
    	return view('website/index');
    }



    ///   SERVICES  ////
    public function servicesnew() {
    	return view('website/services/new');
    }
    public function services() {
    	$services = DB::table('tbl_services')->orderBy('title','ASC')->simplePaginate(20);
    	return view('website/services/index',compact('services'));
    }
    public function servicesedit($id) {
    	$details = DB::table('tbl_services')->where('incre',$id)->limit(1)->get();
      Session::regenerateToken();
    	return view('website/services/edit',compact('details'));
    }
    public function servicesupdate(Request $request, $id) {
    	// update
    	$services= Services::where('incre', $id)->firstOrFail();
    	$services->title = html_entity_decode($request['title']);
  		$services->details = html_entity_decode($request['details']);
  		$services->save();
  		// redirect
      $details = DB::table('tbl_services')->where('incre',$id)->limit(1)->get();
      Session::regenerateToken();
      return view('website/services/edit',compact('details'));

      }










    ///   downloads  ////
    public function downloads() {
    	$downloads = DB::table('tbl_downloads')->orderBy('title','ASC')->simplePaginate(20);
    	return view('website/downloads/index',compact('downloads'));
    }
    public function downloadsnew() {
    	return view('website/downloads/new');
    }
    public function downloadsdelete($id) {
    	DB::table('tbl_downloads')->where('incre',$id)->delete();
    	$downloads = DB::table('tbl_downloads')->orderBy('incre')->simplePaginate(20);
      Session::regenerateToken();
    	return view('website/downloads/index',compact('downloads'));
    }
    public function downloadsadd(Request $request) {
    	// upload image
    	$image = $request->file('attachement');
    	if($image != "") {
    		$image = $image ;
    		$this->validate($request, [
            	'attachement' => 'required|mimes:pdf,zip|max:5000',
	        ]);

	        $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
	        $destinationPath = public_path('library/downloads/');
	        $image->move($destinationPath, $input['imagename']);
    	}

    	// update
    	$downloads= new Downloads;
    	$downloads->title = html_entity_decode($request['title']);
  		$downloads->file = html_entity_decode($input['imagename']);
  		$downloads->save();
      // sync folder contents
      $this->xcopy('public/library/downloads', base_path('../public/library/downloads') ,0755);
  		// redirect
  		$downloads = DB::table('tbl_downloads')->orderBy('incre')->simplePaginate(20);
      	return view('website/downloads/index',compact('downloads'));
      }
        ///   Downloads  ////





        ///   products  ////
    public function products() {
    	$products = DB::table('tbl_products')->orderBy('title','ASC')->simplePaginate(20);
    	return view('website/products/index',compact('products'));
    }
    public function productsnew() {
    	return view('website/products/new');
    }
    public function productsdelete($id) {
    	DB::table('tbl_products')->where('incre',$id)->delete();
      Session::regenerateToken();
    	return redirect('products');
    }
    public function productsedit($id) {
      $products = DB::table('tbl_products')->where('incre',$id)->limit(1)->get();
      return view('website/products/edit',compact('products'));
    }
    public function productsadd(Request $request) {
    	// upload image
    	$image = $request->file('attachement');
    	if($image != "") {
    		$image = $image ;
    		$this->validate($request, [
            	'attachement' => 'required|mimes:jpg,jpeg,gif,png,JPG,JPEG,GIF,PNG|max:5000',
	        ]);

	        $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
	        $destinationPath = public_path('library/products/');
	        $image->move($destinationPath, $input['imagename']);
    	}

    	// update
    	$products= new Products;
    	$products->title = html_entity_decode($request['title']);
      $products->img = html_entity_decode($input['imagename']);
      $products->description = html_entity_decode($request['description']);
      $products->details = $request['details'];
  		$products->save();
      // sync folder contents
      $this->xcopy('public/library/products', base_path('../public/library/products') ,0755);
  		// redirect
  		return redirect('products');
      }
      public function productsupdate(Request $request, $id) {
      // update
      $products = Products::where('incre', $id)->firstOrFail();
      $products->title = html_entity_decode($request['title']);
      $products->description = html_entity_decode($request['description']);
      $products->details = $request['details'];
      $products->save();
      // redirect
      Session::regenerateToken();
      return redirect('products');

      }
        ///   Products  ////




    ///   news  ////
    public function news() {
    	$news = DB::table('tbl_news')->orderBy('title','ASC')->simplePaginate(20);
    	return view('website/news/index',compact('news'));
    }
    public function newsnew() {
    	return view('website/news/new');
    }
    public function newsdelete($id) {
    	DB::table('tbl_news')->where('incre',$id)->delete();
      Session::regenerateToken();
    	return redirect('news');
    }
    public function newsedit($id) {
      $news = DB::table('tbl_news')->where('incre',$id)->limit(1)->get();
      return view('website/news/edit',compact('news'));
    }
    public function newsadd(Request $request) {
    	// upload image
    	$image = $request->file('attachement');
    	if($image != "") {
    		$image = $image ;
    		$this->validate($request, [
            	'attachement' => 'required|mimes:jpg,jpeg,gif,png,JPG,JPEG,GIF,PNG|max:5000',
	        ]);

	        $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
	        $destinationPath = public_path('library/news/');
	        $image->move($destinationPath, $input['imagename']);
    	}

    	// update
    	$news= new News;
    	$news->title = html_entity_decode($request['title']);
      $news->img = html_entity_decode($input['imagename']);
      $news->entrydate = date('Y-m-d', strtotime($request['entrydate']));
      $news->description = html_entity_decode($request['title']);
      $news->details = $request['details'];
  		$news->save();
      // sync folder contents
      $this->xcopy('public/library/news', base_path('../public/library/news') ,0755);
  		// redirect
      return redirect('news');
      }
      public function newsupdate(Request $request, $id) {
      // update
      $news = News::where('incre', $id)->firstOrFail();
      $news->title = html_entity_decode($request['title']);
      $news->description = html_entity_decode($request['description']);
      $news->details = $request['details'];
      $news->save();
      // redirect
      Session::regenerateToken();
      return redirect('news');

      }
        ///   news  ////




        ///   brands  ////
    public function brands() {
    	$brands = DB::table('tbl_brands')->orderBy('title','ASC')->simplePaginate(20);
    	return view('website/brands/index',compact('brands'));
    }
    public function brandsnew() {
    	return view('website/brands/new');
    }
    public function brandsdelete($id) {
    	DB::table('tbl_brands')->where('incre',$id)->delete();
      Session::regenerateToken();
    	return redirect('brands');
    }
    public function brandsadd(Request $request) {
    	// upload image
    	$image = $request->file('attachement');
    	if($image != "") {
    		$image = $image ;
    		$this->validate($request, [
            	'attachement' => 'required|mimes:jpg,jpeg,gif,png,JPG,JPEG,GIF,PNG|max:5000',
	        ]);

	        $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
	        $destinationPath = public_path('library/partners/');
	        $image->move($destinationPath, $input['imagename']);
    	}

    	// update
    	$brands= new Brands;
    	$brands->title = html_entity_decode($request['title']);
      $brands->img = html_entity_decode($input['imagename']);
      $brands->url = $request['url'];
  		$brands->save();
      // sync folder contents
      $this->xcopy('public/library/partners', base_path('../public/library/partners') ,0755);
  		// redirect
      return redirect('brands');
      }

      ///   brands  ////



        /// testimonials ///
        public function testimonialsnew() {
        	return view('website/testimonials/new');
        }
        public function testimonials() {
        	$testimonials = DB::table('tbl_testimonials')->orderBy('incre')->simplePaginate(20);
        	return view('website/testimonials/index',compact('testimonials'));
        }
        public function testimonialsdelete($id) {
        	DB::table('tbl_testimonials')->where('incre',$id)->delete();
        	$testimonials = DB::table('tbl_testimonials')->orderBy('incre')->simplePaginate(20);
          Session::regenerateToken();
        	return view('website/testimonials/index',compact('testimonials'));
        }
        public function testimonialsedit($id) {
        	$details = DB::table('tbl_testimonials')->where('incre',$id)->limit(1)->get();
        	return view('website/testimonials/edit',compact('details'));
        }
        public function testimonialsadd(Request $request) {
          // update
          $testimonials= new Testimonials;
          $testimonials->customer = html_entity_decode($request['customer']);
          $testimonials->details = html_entity_decode($request['details']);
          $testimonials->save();
          // redirect
          $testimonials= DB::table('tbl_testimonials')->orderBy('incre')->simplePaginate(20);
          Session::regenerateToken();
          return view('website/testimonials/index',compact('testimonials'));

        }
        public function testimonialsupdate(Request $request, $id) {

          // update
          $testimonials = Testimonials::where('incre', $id)->firstOrFail();
          $testimonials->customer = html_entity_decode($request['customer']);
          $testimonials->details = html_entity_decode($request['details']);
          $testimonials->save();
          // redirect
          $details = DB::table('tbl_testimonials')->where('incre',$id)->limit(1)->get();
          Session::regenerateToken();
          return view('website/testimonials/edit',compact('details'));

          }

        // NEWS ///





        ///   careers  ////
        public function careers() {
          $careers = DB::table('tbl_careers')->where('incre','1')->simplePaginate(20);
          return view('website/careers/index',compact('careers'));
        }
        public function careersedit($id) {
          $details = DB::table('tbl_careers')->where('incre',$id)->limit(1)->get();
          Session::regenerateToken();
          return view('website/careers/edit',compact('details'));
        }
        public function careersupdate(Request $request, $id) {
          // update
          $careers= Careers::where('incre', $id)->firstOrFail();
          $careers->title = html_entity_decode($request['title']);
          $careers->details = html_entity_decode($request['details']);
          $careers->save();
          // redirect
          Session::regenerateToken();
          $details = DB::table('tbl_careers')->where('incre',$id)->limit(1)->get();
          return view('website/careers/edit',compact('details'));

        }

        // careers //

        ///   About  ////
        public function about() {
          $about = DB::table('tbl_about')->where('incre','1')->simplePaginate(20);
          return view('website/about/index',compact('about'));
        }
        public function aboutedit($id) {
          $details = DB::table('tbl_about')->where('incre',$id)->limit(1)->get();
          Session::regenerateToken();
          return view('website/about/edit',compact('details'));
        }
        public function aboutupdate(Request $request, $id) {
          // update
          $about= About::where('incre', $id)->firstOrFail();
          $about->title = html_entity_decode($request['title']);
          $about->details = html_entity_decode($request['details']);
          $about->save();
          // redirect
          Session::regenerateToken();
          $details = DB::table('tbl_about')->where('incre',$id)->limit(1)->get();
          return view('website/about/edit',compact('details'));

        }

        ///   Contact  ////
        public function contact() {
          $contact = DB::table('tbl_contact')->where('incre','1')->simplePaginate(20);
          return view('website/contact/index',compact('contact'));
        }
        public function contactedit($id) {
          $details = DB::table('tbl_contact')->where('incre',$id)->limit(1)->get();
          Session::regenerateToken();
          return view('website/contact/edit',compact('details'));
        }
        public function contactupdate(Request $request, $id) {
          // update
          $contact= Contact::where('incre', $id)->firstOrFail();
          $contact->title = html_entity_decode($request['title']);
          $contact->details = html_entity_decode($request['details']);
          $contact->save();
          // redirect
          Session::regenerateToken();
          $details = DB::table('tbl_contact')->where('incre',$id)->limit(1)->get();
          return view('website/contact/edit',compact('details'));

        }




      public function xcopy($source, $dest, $permissions = 0755)
      {
          // Check for symlinks
          if (is_link($source)) {
              return symlink(readlink($source), $dest);
          }

          // Simple copy for a file
          if (is_file($source)) {
              return copy($source, $dest);
          }

          // Make destination directory
          if (!is_dir($dest)) {
              mkdir($dest, $permissions);
          }

          // Loop through the folder
          $dir = dir($source);
          while (false !== $entry = $dir->read()) {
              // Skip pointers
              if ($entry == '.' || $entry == '..') {
                  continue;
              }

              // Deep copy directories
              $this->xcopy("$source/$entry", "$dest/$entry", $permissions);
          }

          // Clean up
          $dir->close();
          return true;
      }
}
