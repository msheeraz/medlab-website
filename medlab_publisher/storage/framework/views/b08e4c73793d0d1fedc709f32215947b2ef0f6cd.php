

<?php $__env->startSection('sidenavi'); ?>
  <?php echo $__env->make('website.sidenavi', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
 
<?php $__env->startSection('spc_header'); ?>
    <link href="<?php echo e(URL::to('/')); ?>/public/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo e(URL::to('/')); ?>/public/assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
    <script src="<?php echo e(URL::to('/')); ?>/public/assets/plugins/summernote-master/summernote.js"></script>
    <script src="<?php echo e(URL::to('/')); ?>/public/assets/js/pages/form-elements.js"></script>
    <script src="<?php echo e(URL::to('/')); ?>/public/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo e(URL::to('/')); ?>/public/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('spc_footer'); ?>
  <script type="text/javascript">
         
            $("#litenders").addClass("active");

            $('.date-picker').datepicker({
                orientation: "top auto",
                autoclose: true,
            });
            $('.datetimepicker').datetimepicker();
            setTimeout(function(){ $(".alert").fadeOut("slow"); },3000);


            $('#details').summernote({
             fontNames: ['DEArial', 'Arial Black', 'Comic Sans MS', 'Courier New', 'Merriweather'],
             fontNamesIgnoreCheck: ['Merriweather']
            });
        
  </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

        <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="<?php echo e(URL::to('/')); ?>/tenders/edit/<?php $__currentLoopData = $details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><?php echo e($detail->incre); ?><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>">
        <main class="page-content content-wrap">        
            <div class="navbar">
                <div class="navbar-inner">
                    <div class="sidebar-pusher">
                        <a href="javascript:void(0);" class="waves-effect waves-button waves-classic push-sidebar">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                    <div class="logo-box">
                        <a href="index.html" class="logo-text">Publisher</a>
                    </div><!-- Logo Box -->                    
                    <div class="topmenu-outer">
                        <div class="top-menu">
                            <ul class="nav navbar-nav navbar-right">                                
                                <li>
                                    <a href="login.html" class="log-out waves-effect waves-button waves-classic">
                                        <span><i class="fa fa-sign-out m-r-xs"></i>Log out</span>
                                    </a>
                                </li>
                            </ul><!-- Nav -->
                        </div><!-- Top Menu -->
                    </div>
                </div>
            </div><!-- Navbar -->
            <div class="page-sidebar sidebar">
                <div class="page-sidebar-inner slimscroll">                   
                    <ul class="menu accordion-menu" style="overflow: hidden;">
                        <?php echo $__env->yieldContent('sidenavi'); ?>  
                    </ul>    
                </div><!-- Page Sidebar Inner -->
            </div><!-- Page Sidebar -->
            <div class="page-inner">
                <div id="main-wrapper">
                    <div class="row m-t-md">
                        <div class="col-md-12">
                            <h2>Tenders</h2>
                            <ol class="breadcrumb">
                                <li><a href="<?php echo e(URL::to('/')); ?>/">Home</a></li>
                                <li><a href="<?php echo e(URL::to('/')); ?>/tenders">Tenders</a></li>
                            </ol>
                            <div class="mailbox-content">
                             <div class="panel panel-white">  
                             
                             <div class="error">
                                <?php if(count($errors) > 0): ?>
                                        <div class="alert alert-danger">
                                            <strong>Whoops!</strong><br>There were some problems with your input.<br><br>
                                            <ul>
                                                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <li><?php echo e($error); ?></li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                        </div>
                                 <?php else: ?>
                                    <?php if(Request::isMethod('post')): ?>
                                    <div class="alert alert-success">
                                        <strong>Success</strong><br>Changes Completed<br><br>                                           
                                    </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                               </div>
                               
                                <div class="panel-body">                                       
                                        <div class="form-group">
                                            <label for="title">Title</label>
                                            <input type="text" class="form-control" id="title" required="required" name="title" placeholder="Enter Title" value="<?php $__currentLoopData = $details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><?php echo e($detail->title); ?><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>">
                                        </div>
                                         <div class="form-group">
                                            <label for="department">Department</label>
                                            <input type="text" class="form-control" required="required" name="department" id="department" placeholder="Enter Department" value="<?php $__currentLoopData = $details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><?php echo e($detail->department); ?><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="reference_no">Reference No.</label>
                                            <input type="text" class="form-control" required="required" id="reference_no" name="reference_no" placeholder="Enter Reference" value="<?php $__currentLoopData = $details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><?php echo e($detail->reference_no); ?><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="publishDate">Publish Date</label>
                                            <input type="text" class="form-control date-picker" data-date-format='yyyy-mm-dd' name="publishDate" required="required" id="publishDate" placeholder="Enter Publish Date (YYYY-MM-DD)" value="<?php $__currentLoopData = $details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><?php echo e($detail->date); ?><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>">
                                        </div>
                                         <div class="form-group">
                                            <label for="publishDateTime">Due DateTime</label>
                                            <input type="text" class="form-control datetimepicker"  required="required" name="publishDateTime" id="publishDateTime" placeholder="Enter Due Date and Time (YYYY-MM-DD HH:MM)" value="<?php $__currentLoopData = $details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><?php echo e(Carbon\Carbon::parse($detail->enddatetime)->format('Y-m-d H:i')); ?><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="attachement">Attachement (PDF)</label>
                                            <input id="attachement" name="attachement" type="file" class="form-control">
                                        </div>                                                                                 
                                        <div class="form-group">
                                            <label for="department">Details</label>
                                            <textarea id="details" class="summernote" name="details"><?php $__currentLoopData = $details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><?php echo e($detail->details); ?><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></textarea>
                                        </div>                                        
                                        <input id="previousfile" name="previousfile" type="hidden" class="form-control" value="<?php $__currentLoopData = $details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><?php echo e($detail->file); ?><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>">
                                        <a href="<?php echo e(URL::to('/')); ?>/tenders"><button type="button" class="btn btn-danger pull-right" style="margin-left: 10px;">CANCEL</button></a>
                                        <button type="submit" class="btn btn-primary pull-right">EDIT</button>
                                </div>
                            </div>

                            <?php echo e(csrf_field()); ?>                            

                            </div>
                        </div>
                    </div><!-- Row -->
                </div><!-- Main Wrapper -->
                <div class="page-footer">
                    <p class="no-s"><?=date("Y");?> &copy; Maldives Airports Company Limited.</p>
                </div>
            </div><!-- Page Inner -->
        </main><!-- Page Content -->
        </form>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('masterlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>