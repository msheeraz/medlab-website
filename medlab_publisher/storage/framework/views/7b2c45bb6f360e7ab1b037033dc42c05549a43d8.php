<?php $__env->startSection('sidenavi'); ?>
  <?php echo $__env->make('website.sidenavi', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('spc_header'); ?>
    <link href="<?php echo e(URL::to('/')); ?>/public/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo e(URL::to('/')); ?>/public/assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
    <script src="<?php echo e(URL::to('/')); ?>/public/assets/plugins/summernote-master/summernote.js"></script>
    <script src="<?php echo e(URL::to('/')); ?>/public/assets/js/pages/form-elements.js"></script>
    <script src="<?php echo e(URL::to('/')); ?>/public/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo e(URL::to('/')); ?>/public/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('spc_footer'); ?>
  <script type="text/javascript">
            $("#licareers").addClass("active");
            $('.date-picker').datepicker({
                orientation: "top auto",
                autoclose: true,
            });
            $('.datetimepicker').datetimepicker();
            setTimeout(function(){ $(".alert").fadeOut("slow"); },3000);


            $('#details').summernote({
             fontNames: ['DEArial', 'Arial Black', 'Comic Sans MS', 'Courier New', 'Merriweather'],
             fontNamesIgnoreCheck: ['Merriweather']
            });


  </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

        <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="<?php echo e(URL::to('/')); ?>/careers/edit/<?php $__currentLoopData = $details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><?php echo e($detail->incre); ?><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>">
        <main class="page-content content-wrap">
            <div class="navbar">
                <div class="navbar-inner">
                    <div class="sidebar-pusher">
                        <a href="javascript:void(0);" class="waves-effect waves-button waves-classic push-sidebar">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                    <div class="logo-box">
                        <a href="<?php echo e(URL::to('/')); ?>" class="logo-text">Publisher</a>
                    </div><!-- Logo Box -->
                    <div class="topmenu-outer">
                        <div class="top-menu">
                            <ul class="nav navbar-nav navbar-right">
                                    <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        <?php echo e(Auth::user()->name); ?> <span class="caret"></span>
                                    </a>

                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="<?php echo e(route('logout')); ?>"
                                                onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                                Logout
                                            </a>

                                            <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                                <?php echo e(csrf_field()); ?>

                                            </form>
                                        </li>
                                    </ul>
                                </li>
                            </ul><!-- Nav -->
                        </div><!-- Top Menu -->
                    </div>
                </div>
            </div><!-- Navbar -->
            <div class="page-sidebar sidebar">
                <div class="page-sidebar-inner slimscroll">
                    <ul class="menu accordion-menu" style="overflow: hidden;">
                        <?php echo $__env->yieldContent('sidenavi'); ?>
                    </ul>
                </div><!-- Page Sidebar Inner -->
            </div><!-- Page Sidebar -->
            <div class="page-inner">
                <div id="main-wrapper">
                    <div class="row m-t-md">
                        <div class="col-md-12">
                            <h2>Careers</h2>
                            <ol class="breadcrumb">
                                <li><a href="<?php echo e(URL::to('/')); ?>/">Home</a></li>
                                <li><a href="<?php echo e(URL::to('/')); ?>/careers">Careers</a></li>
                            </ol>
                            <div class="mailbox-content" style="min-height:714px">
                             <div class="panel panel-white">

                             <div class="error">
                                <?php if(count($errors) > 0): ?>
                                        <div class="alert alert-danger">
                                            <strong>Whoops!</strong><br>There were some problems with your input.<br><br>
                                            <ul>
                                                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <li><?php echo e($error); ?></li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                        </div>
                                 <?php else: ?>
                                    <?php if(Request::isMethod('post')): ?>
                                    <div class="alert alert-success">
                                        <strong>Success</strong><br>Changes Completed<br><br>
                                    </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                               </div>


                                   <div class="panel-body">
                                           <div class="form-group">
                                               <label for="title">Airport</label>
                                               <input type="text" class="form-control" id="title" readonly="readonly" required="required" name="title" placeholder="Enter Title" value="<?php $__currentLoopData = $details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><?php echo e($detail->title); ?><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>">
                                           </div>

                                           <div class="form-group">
                                               <label for="department">Template</label>
                                               <select name="template" class="form-control" id="template">
                                                    <option value="1">No Vacancies</option>
                                                    <option value="2">New Vacancy Template</option>
                                               </select>
                                           </div>

                                           <div class="form-group">
                                               <label for="department">Details</label>
                                               <textarea id="details" class="summernote" name="details"><?php $__currentLoopData = $details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $detail): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?><?php echo e($detail->details); ?><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?></textarea>
                                           </div>
                                           <a href="<?php echo e(URL::to('/')); ?>/aip"><button type="button" class="btn btn-danger pull-right" style="margin-left: 10px;">CANCEL</button></a>
                                           <button type="submit" class="btn btn-primary pull-right">UPDATE</button>
                                   </div>
                            </div>

                            <?php echo e(csrf_field()); ?>


                            </div>
                        </div>
                    </div><!-- Row -->
                </div><!-- Main Wrapper -->
                <div class="page-footer">
                    <p class="no-s"><?=date("Y");?> &copy; Mohamed Sheeraz.</p>
                </div>
            </div><!-- Page Inner -->
        </main><!-- Page Content -->
        </form>
        <script>

        $(document).on('change','#template',function(){
            var refId = $(this).val();
            var noVacancies = "<div style='min-height:400px'><p>There are no vacancies open at the moment, check back later!</p></div>";
            var yesVacancies = '<table class="table table-bordered"><tbody><tr><td><span style="font-weight: bold;">Job Position</span></td><td>[ Job Position, Senior Administrator]</td></tr><tr><td><span style="font-weight: bold;">Job Reference</span></td><td>[ Job Reference, JB0002154]</td></tr><tr><td><span style="font-weight: bold;">Vacancies</span></td><td>[ How many openings, 03]</td></tr><tr><td><span style="font-weight: bold;">Requirements</span></td><td>[ Requirement details]</td></tr><tr><td><span style="font-weight: bold;">Work Summary</span></td><td>[ work Summary]&nbsp;</td></tr><tr><td><span style="font-weight: bold;">Salary</span></td><td>[ Salary details]</td></tr><tr><td><span style="font-weight: bold;">Deadline</span></td><td>[ Deadline date with time, 02 Aug 2017, 1430hrs] &nbsp;</td></tr><tr><td><span style="font-weight: bold;">Contact</span></td><td>[ Contact details, phone : 3333333, email : info@domain.com]</td></tr><tr><td><span style="font-weight: bold;">Attachment</span></td><td>[ Job Details attachment after adding to download section, http://localhost/medlab/public/library/downloads/1506078371.pdf]</td></tr></tbody></table>';
            $(".note-editable").html("");
            if(refId == 1) {
                $(".note-editable").html(noVacancies);
            } else {
                $(".note-editable").html(yesVacancies);
            }

        })
        </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('masterlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>