<?php $__env->startSection('sidenavi'); ?>
        <li data-toggle="tooltip" data-placement="right" title="Services" id="liservices"><a href="<?php echo e(URL::to('/')); ?>/services" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-plane"></span><p>Services</p></a></li>
        <li data-toggle="tooltip" data-placement="right" title="Downloads" id="lidownloads"><a href="<?php echo e(URL::to('/')); ?>/downloads" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-download-alt"></span><p>Downloads</p></a></li>
        <li data-toggle="tooltip" data-placement="right" title="Testimonials" id="litestimonials"><a href="<?php echo e(URL::to('/')); ?>/testimonials" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-comment"></span><p>Testimonials</p></a></li>
        <li data-toggle="tooltip" data-placement="right" title="AIP" id="lipaip" ><a href="<?php echo e(URL::to('/')); ?>/aip" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-send"></span><p>AIP</p></a></li>
        <li data-toggle="tooltip" data-placement="right" title="Maldives" id="lipmaldives" ><a href="<?php echo e(URL::to('/')); ?>/maldives" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-tree-conifer"></span><p>Maldives</p></a></li>
        <li data-toggle="tooltip" data-placement="right" title="About" id="lipabout" ><a href="<?php echo e(URL::to('/')); ?>/about" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-map-marker"></span><p>About Us</p></a></li>
        <li data-toggle="tooltip" data-placement="right" title="Contact" id="lipcontact" ><a href="<?php echo e(URL::to('/')); ?>/contact" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-phone-alt"></span><p>Contact Us</p></a></li>
    </ul>
<?php $__env->stopSection(); ?>
