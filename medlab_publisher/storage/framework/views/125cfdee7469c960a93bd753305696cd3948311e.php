<!DOCTYPE html>
<html>
    <head>

        <!-- Title -->
        <title>AVIA | Publisher</title>

        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta charset="UTF-8">
        <meta name="description" content="AVIA | Publisher" />
        <!-- Styles -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
        <link href="<?php echo e(URL::to('/')); ?>/public/assets/plugins/pace-master/themes/blue/pace-theme-flash.css" rel="stylesheet"/>
        <link href="<?php echo e(URL::to('/')); ?>/public/assets/plugins/uniform/css/uniform.default.min.css" rel="stylesheet"/>
        <link href="<?php echo e(URL::to('/')); ?>/public/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo e(URL::to('/')); ?>/public/assets/plugins/fontawesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo e(URL::to('/')); ?>/public/assets/plugins/line-icons/simple-line-icons.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo e(URL::to('/')); ?>/public/assets/plugins/offcanvasmenueffects/css/menu_cornerbox.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo e(URL::to('/')); ?>/public/assets/plugins/waves/waves.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo e(URL::to('/')); ?>/public/assets/plugins/switchery/switchery.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo e(URL::to('/')); ?>/public/assets/plugins/3d-bold-navigation/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo e(URL::to('/')); ?>/public/assets/plugins/slidepushmenus/css/component.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo e(URL::to('/')); ?>/public/assets/plugins/summernote-master/summernote.css" rel="stylesheet" type="text/css"/>

        <!-- Theme Styles -->
        <link href="<?php echo e(URL::to('/')); ?>/public/assets/css/modern.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo e(URL::to('/')); ?>/public/assets/css/themes/white.css" class="theme-color" rel="stylesheet" type="text/css"/>
        <link href="<?php echo e(URL::to('/')); ?>/public/assets/css/custom.css" rel="stylesheet" type="text/css"/>

        <script src="<?php echo e(URL::to('/')); ?>/public/assets/plugins/jquery/jquery-2.1.3.min.js"></script>
        <script src="<?php echo e(URL::to('/')); ?>/public/assets/plugins/3d-bold-navigation/js/modernizr.js"></script>
        <script src="<?php echo e(URL::to('/')); ?>/public/assets/plugins/offcanvasmenueffects/js/snap.svg-min.js"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php echo $__env->yieldContent('spc_header'); ?>


    </head>
    <body class="page-header-fixed  pace-done">
        <?php echo $__env->yieldContent('content'); ?>
        <?php echo $__env->yieldContent('spc_footer'); ?>
        <!-- Javascripts -->
        <script src="<?php echo e(URL::to('/')); ?>/public/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
        <script src="<?php echo e(URL::to('/')); ?>/public/assets/plugins/pace-master/pace.min.js"></script>
        <script src="<?php echo e(URL::to('/')); ?>/public/assets/plugins/jquery-blockui/jquery.blockui.js"></script>
        <script src="<?php echo e(URL::to('/')); ?>/public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo e(URL::to('/')); ?>/public/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="<?php echo e(URL::to('/')); ?>/public/assets/plugins/switchery/switchery.min.js"></script>
        <script src="<?php echo e(URL::to('/')); ?>/public/assets/plugins/uniform/jquery.uniform.min.js"></script>
        <script src="<?php echo e(URL::to('/')); ?>/public/assets/plugins/offcanvasmenueffects/js/classie.js"></script>
        <script src="<?php echo e(URL::to('/')); ?>/public/assets/plugins/offcanvasmenueffects/js/main.js"></script>
        <script src="<?php echo e(URL::to('/')); ?>/public/assets/plugins/waves/waves.min.js"></script>
        <script src="<?php echo e(URL::to('/')); ?>/public/assets/plugins/3d-bold-navigation/js/main.js"></script>
        <script src="<?php echo e(URL::to('/')); ?>/public/assets/js/modern.js"></script>
        <script src="<?php echo e(URL::to('/')); ?>/public/assets/js/pages/inbox.js"></script>



    </body>
</html>
