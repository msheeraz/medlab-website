

<?php $__env->startSection('sidenavi'); ?>
  <?php echo $__env->make('website.sidenavi', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('spc_header'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('spc_footer'); ?>
  <script type="text/javascript">
            $("#lipages").addClass("active");
            $(document).on("click","#btnDelete",function(){
                var title = $(this).attr("pr_data");
                var refId = $(this).attr("pn_data");
                $("#reftitle").text(title);
                $("#reftarget").attr("href","<?php echo e(URL::to('/')); ?>/pages/delete/"+refId+"");
                $("#modalDeleteConfirm").modal("show");
            })

  </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

        <div class="modal fade" id="modalDeleteConfirm" tabindex="-1" role="dialog" aria-labelledby="Label" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Delete Confirmation</h4>
                    </div>
                    <div class="modal-body">
                        This will delete <strong><span id="reftitle"></span></strong><br>
                        Would you like to proceed?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <a href="#" id="reftarget"><button type="button" class="btn btn-danger">Delete</button></a>
                    </div>
                </div>
            </div>
        </div>

        <main class="page-content content-wrap">
            <div class="navbar">
                <div class="navbar-inner">
                    <div class="sidebar-pusher">
                        <a href="javascript:void(0);" class="waves-effect waves-button waves-classic push-sidebar">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                    <div class="logo-box">
                        <a href="index.html" class="logo-text">Publisher</a>
                    </div><!-- Logo Box -->
                    <div class="topmenu-outer">
                        <div class="top-menu">
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <a href="login.html" class="log-out waves-effect waves-button waves-classic">
                                        <span><i class="fa fa-sign-out m-r-xs"></i>Log out</span>
                                    </a>
                                </li>
                            </ul><!-- Nav -->
                        </div><!-- Top Menu -->
                    </div>
                </div>
            </div><!-- Navbar -->
            <div class="page-sidebar sidebar">
                <div class="page-sidebar-inner slimscroll">
                    <ul class="menu accordion-menu" style="overflow: hidden;">
                        <?php echo $__env->yieldContent('sidenavi'); ?>
                    </ul>
                </div><!-- Page Sidebar Inner -->
            </div><!-- Page Sidebar -->
            <div class="page-inner">
                <div id="main-wrapper">
                    <div class="row m-t-md">
                        <div class="col-md-12">
                            <h2>Pages</h2>
                            <div class="mailbox-content">
                            <div id="cont_pagination" class="pull-right">
                                <div style="float: left; width: 180px;"> <?php echo e($pages->links()); ?> </div>
                                 <div style="float: right; text-align: right; font-size: 15px; margin-top: 25px; margin-right: 10px; width: 127px; font-weight: bold">Showing <?php echo e(($pages->currentpage()-1)*$pages->perpage()+1); ?> to <?php echo e($pages->currentpage()*$pages->perpage()); ?></div>
                             </div>
                             <a href="<?php echo e(URL::to('/')); ?>/pages/new"><button type="button" class="btn btn-primary btn-addon m-b-sm"><i class="fa fa-plus"></i> Add New Pages</button></a>
                            <table class="table">
                                <thead style="font-weight: bold; font-size: 18px">
                                      <td>Title</td>
                                      <td>Area</td>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $details): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr class="unread">
                                        <td class="hidden-xs">
                                              <strong><?php echo e($details->title); ?></strong>
                                        </td>
                                        <td>
                                          <?php if($details->area == "D"): ?>
                                            <?php echo e($area = "Development"); ?>

                                          <?php elseif($details->area == "P"): ?>
                                            <?php echo e($area = "Passenger"); ?>

                                          <?php else: ?>
                                            <?php echo e($area = "Corporate"); ?>

                                          <?php endif; ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo e(URL::to('/')); ?>/pages/edit/<?php echo e($details->incre); ?>"><button type="button" class="btn btn-primary">EDIT</button></a>
                                            <button type="button" id="btnDelete" class="btn btn-danger" pn_data="<?php echo e($details->incre); ?>" pr_data="<?php echo e($details->title); ?>">DELETE</button>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div><!-- Row -->
                </div><!-- Main Wrapper -->
                <div class="page-footer">
                    <p class="no-s"><?=date("Y");?> &copy; Maldives Airports Company Limited.</p>
                </div>
            </div><!-- Page Inner -->
        </main><!-- Page Content -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('masterlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>