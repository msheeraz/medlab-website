<?php $__env->startSection('sidenavi'); ?>
        <li data-toggle="tooltip" data-placement="right" title="Tenders" id="litenders"><a href="<?php echo e(URL::to('/')); ?>/tenders" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-home"></span><p>Tenders</p></a></li>
        <li data-toggle="tooltip" data-placement="right" title="Jobs" id="lijobs"><a href="<?php echo e(URL::to('/')); ?>/jobs" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-home"></span><p>Jobs</p></a></li>
        <li data-toggle="tooltip" data-placement="right" title="News" id="linews"><a href="<?php echo e(URL::to('/')); ?>/news" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-home"></span><p>News</p></a></li>
        <li data-toggle="tooltip" data-placement="right" title="Pages" id="lipages" ><a href="<?php echo e(URL::to('/')); ?>/pages" class="waves-effect waves-button"><span class="menu-icon glyphicon glyphicon-user"></span><p>Pages</p></a></li>
    </ul>
<?php $__env->stopSection(); ?>
